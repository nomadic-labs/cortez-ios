// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

public protocol Disposable {
	func dispose()
}

public struct AnyDisposable: Disposable {
	
	public typealias Body = () -> Void
	
	private let body: Body
	
	public init(_ body: @escaping Body) {
		self.body = body
	}
	public init<T: Disposable>(_ base: T) {
		self.init(base.dispose)
	}
	
	public func dispose() {
		body()
	}
}

public extension AnyDisposable {
	static var empty = AnyDisposable {}
}

final class AutoDisposable: Disposable {

	let semaphore = DispatchSemaphore(value: 1)
	var base: Disposable?
	
	init(_ base: Disposable) {
		self.base = base
	}
	deinit {
		dispose()
	}
	
	public func dispose() {
		semaphore.wait(); defer { semaphore.signal() }
		base?.dispose()
		base = nil
	}
}

public extension Disposable {
	func autodispose() -> Disposable {
		return AutoDisposable(self)
	}
}
