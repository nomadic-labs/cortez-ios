// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

struct Async<Value>: Completable {
	
	private let work: Work<Value>
	
	init(_ work: @escaping Work<Value>) {
		self.work = work
	}
	
	func start(_ completion: @escaping Completion<Value>) {
		guard let scope = AnyExecutor.current else {
			completion(.failure(Error.outOfScope))
			return
		}
		scope.async(execute: work, notify: completion)
	}
}

func runBlocking(scope: Executor, start: @escaping () -> Void, promise: AnyPromise<Void> = AnyPromise { _ in }) {
	AnyExecutor.current = scope
	start()
	promise.resolve(with: ())
	AnyExecutor.current = nil
}

public func async(queue: DispatchQueue = .global(), scope: Disposable & Executor, start: @escaping () -> Void) {
	Async<Void> { promise in
		queue.async {
			runBlocking(scope: scope, start: start, promise: promise)
		}
		return scope
	}.start()
}

public func await<T>(_ work: @escaping Work<T>) throws -> T {
	return try Async(work).await()
}

public func race<T>(_ works: [Work<T>]) -> AnyCompletable<T> {
	return AnyCompletable {
		let semaphore = DispatchSemaphore(value: 1)
		var completion = Optional($0)
		let supervisor = { (result: Result<T, Swift.Error>) in
			semaphore.wait(); defer { semaphore.signal() }
			completion?(result)
			completion = nil
		}
		works.map(Async.init).forEach { $0.start(supervisor) }
	}
}
public func race<T>(_ works: Work<T>...) throws -> AnyCompletable<T> {
	return race(works)
}

@discardableResult
public func launch(queue: DispatchQueue = .global(), start: @escaping () -> Void) -> Disposable {
	let scope = Scope()
	if AnyExecutor.current == nil {
		queue.async { runBlocking(scope: scope, start: start) }
	} else {
		async(queue: queue, scope: scope, start: start)
	}
	return scope
}
