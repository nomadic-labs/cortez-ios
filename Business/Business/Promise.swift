// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

public protocol Promise {
	
	associatedtype Value
	
	func fulfill(with result: Result<Value, Swift.Error>)
}

public extension Promise {
	func resolve(with value: Value) {
		fulfill(with: .success(value))
	}
	func reject(with error: Swift.Error) {
		fulfill(with: .failure(error))
	}
}

public extension Promise where Value == Void {
	func fulfill(with error: Swift.Error?) {
		fulfill(with: error.map { .failure($0) } ?? .success(()))
	}
}

public struct AnyPromise<Value>: Promise {
	
	public typealias Body = (Result<Value, Swift.Error>) -> Void
	
	private let body: Body
	
	public init(_ body: @escaping Body) {
		self.body = body
	}
	public init<T: Promise>(_ base: T) where Value == T.Value {
		self.init(base.fulfill)
	}
	
	public func fulfill(with result: Result<Value, Swift.Error>) {
		body(result)
	}
}
