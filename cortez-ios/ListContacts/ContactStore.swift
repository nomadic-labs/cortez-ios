// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation
import CoreData

struct Contact {
	var name: String
	var address: String
}

extension Contact: Storable {

	enum Predicate {
		case all
		case addressEquals(String)
		case addressIn([String])
		case nameContains(String)
		case nameEquals(String)
	}

	enum Attribute: String {
		case indexTitle
		case name
	}
}

extension Contact.Attribute: Named {
	var name: String { rawValue }
}

extension Contact: Hashable {
	static func ==(lhs: Contact, rhs: Contact) -> Bool {
		return lhs.address == rhs.address
	}
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(address)
	}
}

typealias ContactStoringLogic = AnyStore<Contact>

extension ContactDTO {
	static func fetchRequest(with predicate: NSPredicate?) -> NSFetchRequest<ContactDTO> {
		let request: NSFetchRequest<ContactDTO> = fetchRequest()
		request.predicate = predicate
		return request
	}
}

extension Contact: CoreDataBacked {

	var request: NSFetchRequest<ContactDTO> {
		return Predicate.addressEquals(address).request
	}

	init(from dto: ContactDTO) {
		self.init(
			name: dto.name ?? "",
			address: dto.address ?? ""
		)
	}
	
	func encode(to dto: ContactDTO) {
		dto.name = name
		dto.address = address
		dto.indexTitle = name.indexTitle
	}
}

extension Contact.Predicate: FetchRequestConvertible {
	var request: NSFetchRequest<ContactDTO> {
		let request: NSFetchRequest<ContactDTO> = ContactDTO.fetchRequest()
		switch self {
		case .all:
			break
		case let .addressEquals(address):
			request.predicate = NSPredicate(format: "address = %@", address)
		case let .addressIn(addresses):
			request.predicate = NSPredicate(format: "address IN %@", addresses)
		case let .nameContains(substring):
			request.predicate = NSPredicate(format: "name CONTAINS[cd] %@", substring)
		case let .nameEquals(string):
			request.predicate = NSPredicate(format: "name = %@", string)
		}
		return request
	}
}
