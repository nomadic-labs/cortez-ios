// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ListContactsDisplayLogic: class {
	func show(_ viewModel: Contact.List.ViewModel)
}

class ListContactsViewController: UIViewController, OnboardPageViewController {

	var router: (OnboardPageRoutingLogic & OnboardPageDataPassingLogic)?
	var interactor: ListContactsBusinessLogic?
	
	@IBOutlet private weak var searchBar: UISearchBar!
	@IBOutlet private weak var contactListView: UITableView!

	var selection: AnySelection<Contact.List.Item>?
	
	let cellSelectedBackgroundView: UIView = {
		let view = UIView()
		view.backgroundColor = .init(white: 0, alpha: 0.2)
		return view
	}()
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}

	private func setup() {
		let router = ListContactsRouter()
		let interactor = ListContactsInteractor(
			presenter: ListContactsPresenter(display: self)
		)
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}


    override func viewDidLoad() {
        super.viewDidLoad()

		// Do any additional setup after loading the view.
		if let parent = parent as? UINavigationController, parent.viewControllers.count > 1 {
			navigationItem.leftBarButtonItem = nil
		}
		setSelectionDelegate()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		onboardController?.isNextButtonHidden = true
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		DispatchQueue.main.async {
			self.searchBar.becomeFirstResponder()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		searchBar.resignFirstResponder()
	}

	private func setSelectionDelegate() {
		selection?.delegate = AnySelectionDelegate(SelectionTableViewControl(tableView: contactListView))
	}

    // MARK: - Navigation
	@IBAction func unwindToListContacts(segue: UIStoryboardSegue) {
		// nothing to do
	}
	@IBAction func unwind(segue: UIStoryboardSegue) {
		// nothing to do
	}
}

extension ListContactsViewController: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return selection?.count ?? 0
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return selection?[section].count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "contact", for: indexPath)
		cell.textLabel?.text = selection?[indexPath].name
		cell.selectedBackgroundView = cellSelectedBackgroundView
		return cell
	}
	
	func sectionIndexTitles(for tableView: UITableView) -> [String]? {
		return selection?.indexTitles
	}
}

extension ListContactsViewController: UITableViewDelegate {
	
	//func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
	//	return CGFloat.leastNormalMagnitude
	//}
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return selection?[section].indexTitle ?? ""
	}

	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
	}

	func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
		guard let view = view as? UITableViewHeaderFooterView else { return }
		view.textLabel?.textColor = .darkGray
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let indexPath = tableView.indexPathsForSelectedRows?.first else { return }
		interactor?.selectContact(at: indexPath)
		router?.routeToNextPage(with: nil)
	}
}

extension ListContactsViewController: UISearchBarDelegate {
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		guard let substring = searchBar.text else { return }
		interactor?.execute(.init(query: substring))
	}
}

extension ListContactsViewController {
	class func instantiateFromStoryboard() -> ListContactsViewController? {
		let storyboard = UIStoryboard(name: "ListContacts", bundle: nil)
		return storyboard.instantiateInitialViewController() as? ListContactsViewController
	}
}

extension ListContactsViewController: ListContactsDisplayLogic {
	func show(_ viewModel: Contact.List.ViewModel) {
		selection = viewModel.selection
		if isViewLoaded {
			setSelectionDelegate()
			contactListView.reloadData()
		}
	}
}
