// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************


import UIKit

final class CreateWalletRouter: OnboardRouter {

	enum Form {
		case create, restore
		
		var storyboardIDs: [String] {
			switch self {
			case .create: return [ "backup-phrase", "create-wallet", "encrypt-wallet" ]
			case .restore: return [ "restore-wallet", "encrypt-wallet" ]
			}
		}
	}
	
	convenience init(_ form: Form) {
		let storyboard = UIStoryboard(name: "CreateWallet", bundle: nil)
		let pageViewControllers = form.storyboardIDs
			.compactMap(storyboard.instantiateViewController)
			.compactMap { $0 as? OnboardPageViewController }
		self.init(pageViewControllers)
		dataStore = CreateWalletInteractor()
	}
	
	override func passData(from source: OnboardDataStore, to destination: inout OnboardPageDataStore) {
		guard let source = source as? CreateWalletDataStore else { return }
		switch destination {
		case var destination as CreateBackupPhraseDataStore:
			passData(from: source, to: &destination)
		case var destination as ConfirmBackupPhraseDataStore:
			passData(from: source, to: &destination)
		case var destination as EncryptWalletDataStore:
			passData(from: source, to: &destination)
		default:
			break
		}
	}
	func passData(from source: CreateWalletDataStore, to destination: inout CreateBackupPhraseDataStore) {
		destination.backupPhrase = source.backupPhrase
	}
	func passData(from source: CreateWalletDataStore, to destination: inout ConfirmBackupPhraseDataStore) {
		destination.backupPhrase = source.backupPhrase
	}
	func passData(from source: CreateWalletDataStore, to destination: inout EncryptWalletDataStore) {
		destination.backupPhrase = source.backupPhrase
	}
}
