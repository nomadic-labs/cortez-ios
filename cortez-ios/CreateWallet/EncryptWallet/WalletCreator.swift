// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import KeychainAccess
import LocalAuthentication
import MnemonicKit

extension String {
	static let defaultMainAccountName = "Main Account"
}

protocol WalletCreationLogic {
	func create(withBackupPhrase backupPhrase: String, password: String, completion: @escaping (Swift.Error?) -> Void)
}

struct WalletCreator: WalletCreationLogic {
	func create(withBackupPhrase backupPhrase: String, password: String, completion: @escaping (Swift.Error?) -> Void) {
		do {
			guard let credential = password.data(using: .utf8) else { return }
			guard let seed = Mnemonic.deterministicSeedString(from: backupPhrase) else { return }
			guard let keyPair = KeyPair(from: String(seed[..<seed.index(seed.startIndex, offsetBy: 64)])) else { return }
			var keychain = Keychain()
			try keychain.set(keyPair.publicKeyData, key: "public-key")
			#if arch(i386) || arch(x86_64)
			#else
			let context = LAContext()
			context.setCredential(credential, type: .applicationPassword)
			keychain = keychain
				.accessibility(.whenUnlockedThisDeviceOnly, authenticationPolicy: .applicationPassword)
				.authenticationContext(context)
			#endif
			try keychain.set(backupPhrase, key: "backup-phrase")
			try keychain.set(keyPair.secretKeyData, key: "secret-key")
			addMyself(with: keyPair)
			completion(nil)
			NotificationCenter.default.post(name: Cortez.userDidLogin, object: self)
		} catch {
			completion(error)
		}
	}
	
	private func addMyself(with keyPair: KeyPair) {
		guard let tz1 = keyPair.tz1 else { return }
		guard let accounts: AnyStore<Account> = Provider.shared.get() else { return }
		guard let contacts: AnyStore<Contact> = Provider.shared.get() else { return }
		accounts.enqueue {
			let myself = Account(index: 0, address: tz1)
			do {
				try $0.insert(myself)
			} catch {
				try? $0.update(myself)
			}
		}
		contacts.enqueue {
			let myself = Contact(name: .defaultMainAccountName, address: tz1)
			try? (try? $0.select(Query.where(.nameEquals(.defaultMainAccountName)).sorted(by: .name, ascending: true)))?.forEach($0.delete)
			do {
				try $0.insert(myself)
			} catch {
				try? $0.update(myself)
			}
		}
	}
}
