// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ConfirmBackupPhraseDisplayLogic: class {
	func show(_ viewModel: Wallet.BackupPhrase.ViewModel)
	func show(_ viewModel: Wallet.BackupPhrase.Validation.ViewModel)
}

class ConfirmBackupPhraseViewController: UIViewController, OnboardPageViewController {

	var router: (OnboardPageRoutingLogic & OnboardPageDataPassingLogic)?
	var interactor: ConfirmBackupPhraseBusinessLogic?

	@IBOutlet private weak var backupPhraseView: UICollectionView!

	private var words = [[String]](arrayLiteral: [], [])
	private var sizes = [[CGSize]](arrayLiteral: [], [])

	let valueViewDummy: UILabel = {
		let view = UILabel()
		view.font = .systemFont(ofSize: 17)
		return view
	}()
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = ConfirmBackupPhraseRouter()
		let interactor = ConfirmBackupPhraseInteractor(
			validator: BackupPhraseComparator(),
			presenter: ConfirmBackupPhrasePresenter(display: self)
		)
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		backupPhraseView.collectionViewLayout = BackupPhraseViewLayout()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		interactor?.execute(.init(backupPhrase: words[0].joined(separator: " ")))
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		backupPhraseView.collectionViewLayout.invalidateLayout()
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ConfirmBackupPhraseViewController: UICollectionViewDataSource {
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 2
	}
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return words[section].count
	}
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "word", for: indexPath)
		if let cell = cell as? WordItemView {
			cell.indexView.text = indexPath.section == 0 ? "\(indexPath.item + 1)" : nil
			cell.valueView.text = words[indexPath.section][indexPath.item]
		}
		return cell
	}
}

extension ConfirmBackupPhraseViewController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let word = words[indexPath.section][indexPath.row]
		let destinationSection = indexPath.section ^ 1
		let destinationItem = words[destinationSection].count
		let destination = IndexPath(item: destinationItem, section: destinationSection)
		words[destinationSection].append(words[indexPath.section].remove(at: indexPath.item))
		sizes[indexPath.section].remove(at: indexPath.item)
		sizes[destinationSection].append(size(for: word, at: destination))
		
		collectionView.performBatchUpdates({
			collectionView.moveItem(at: indexPath, to: destination)
		}, completion: { _ in
			UIView.animate(withDuration: -1) {
				collectionView.invalidateIntrinsicContentSize()
				self.view.setNeedsLayout()
				self.view.layoutIfNeeded()
			}
			collectionView.performBatchUpdates({
				collectionView.reloadItems(at: [destination])
			}, completion: { _ in
				self.interactor?.execute(.init(backupPhrase: self.words[0].joined(separator: " ")))
			})
		})
	}
}

extension ConfirmBackupPhraseViewController: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return sizes[indexPath.section][indexPath.row]
	}
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		if section == 0 || words[0].isEmpty {
			return .zero
		}
		return CGSize(width: 0, height: 32)
	}
}

extension ConfirmBackupPhraseViewController: ConfirmBackupPhraseDisplayLogic {
	func show(_ viewModel: Wallet.BackupPhrase.ViewModel) {

		words = [[], viewModel.words]
		sizes = [
			[],
			viewModel.words.enumerated().map { self.size(for: $1, at: IndexPath(item: $0, section: 1)) }
		]
		
		if isViewLoaded {
			backupPhraseView.reloadData()
		}
	}

	func show(_ viewModel: Wallet.BackupPhrase.Validation.ViewModel) {
		onboardController?.isNextButtonEnabled = viewModel.success
	}
	
	private func size(for word: String, at indexPath: IndexPath) -> CGSize {
		valueViewDummy.text = word
		valueViewDummy.sizeToFit()
		return CGSize(width: 32 + valueViewDummy.frame.width + 12, height: 32)
	}
}
