// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import MnemonicKit

protocol BackupPhraseValidationLogic {
	func validate(_ backupPhrase: String, completion: @escaping (Bool) -> Void)
}

struct BackupPhraseValidator: BackupPhraseValidationLogic {
	
	private let mnemonicWords = SortedArray(MnemonicLanguage.english.words)
	
	func validate(_ backupPhrase: String, completion: @escaping (Bool) -> Void) {
		let words = backupPhrase.components(separatedBy: .whitespacesAndNewlines)
		let wordIndexes = words.reduce(into: [Int]()) {
			let (index, found) = mnemonicWords.insertIndex(for: String($1))
			if found {
				$0.append(index)
			}
		}
		completion(validate(wordIndexes))
	}
	
	private func validate(_ wordIndexes: [Int]) -> Bool {
		if wordIndexes.isEmpty || wordIndexes.count > 24 {
			return false
		}
		
		let entPlusCs = wordIndexes.count * 11
		let ent = (entPlusCs * 32) / 33
		let cs = ent / 32
		if entPlusCs != ent + cs {
			return false // word count exception
		}
		
		var entropyWithChecksum = Data(repeating: 0, count: (entPlusCs + 7) / 8)
		entropyWithChecksum.writeEntropyWithChecksum(of: wordIndexes)
		
		let entropy = entropyWithChecksum[0..<(entropyWithChecksum.count - 1)]
		let lastByte = entropyWithChecksum.last!
		
		let sha = entropy.firstSHA256Byte!
		
		let mask: Byte = .firstNBitsMask(cs)

		return ((sha ^ lastByte) & mask) == 0 // checksum exception
	}
}

private extension Data {
	//firstByteOfSha256
	var firstSHA256Byte: Byte? {
		return sha256().first
	}
	
	//wordIndexesToEntropyWithCheckSum
	mutating func writeEntropyWithChecksum(of wordIndexes: [Int]) {
		var bi = 0
		for wordIndex in wordIndexes {
			writeNext11(wordIndex, at: bi)
			bi += 11
		}
	}
	mutating func writeNext11(_ value: Int, at offset: Int) {
		let skip = offset / 8
		let bitSkip = offset % 8
		
		//byte 0
		let firstValue = self[skip]
		let toWrite0 = Byte((value >> (3 + bitSkip)) & 0xff)
		self[skip] = Byte(firstValue | toWrite0)
		
		//byte 1
		let valueInByte = self[skip + 1]
		let i = 5 - bitSkip
		let toWrite1 = Byte((i > 0 ? (value << i) : (value >> -i)) & 0xff)
		self[skip + 1] = Byte(valueInByte | toWrite1)
		
		//byte 2
		if bitSkip >= 6 {
			let valueInByte = self[skip + 2]
			let toWrite = Byte((value << (13 - bitSkip)) & 0xff)
			self[skip + 2] = Byte(valueInByte | toWrite)
		}
	}
}
