// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import MnemonicKit

protocol BackupPhraseWordSuggestionLogic {
	func suggestWords(forPrefix prefix: String, completion: @escaping ([String]) -> Void)
}

struct BackupPhraseWordSuggester: BackupPhraseWordSuggestionLogic {
	
	private let mnemonicWords = SortedArray(MnemonicLanguage.english.words)
	
	func suggestWords(forPrefix prefix: String, completion: @escaping ([String]) -> Void) {
		_ = DispatchQueue.global(qos: .userInteractive).async {
			let (index, _) = self.mnemonicWords.insertIndex(for: prefix)
			let range = index...
			let words = self.mnemonicWords[range].takeWhile { $0 < 3 && $1.hasPrefix(prefix) }
			DispatchQueue.main.async { completion(words) }
		}
	}
}
