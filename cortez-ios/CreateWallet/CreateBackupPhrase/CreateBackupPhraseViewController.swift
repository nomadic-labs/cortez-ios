// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol CreateBackupPhraseDisplayLogic: class {
	func show(_ viewModel: Wallet.BackupPhrase.ViewModel)
}

class CreateBackupPhraseViewController: UIViewController, OnboardPageViewController {

	var router: (OnboardPageRoutingLogic & OnboardPageDataPassingLogic)?
	var interactor: CreateBackupPhraseBusinessLogic?

	@IBOutlet private weak var backupPhraseView: UICollectionView!

	private var words = [String]()
	private var sizes = [CGSize]()

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = CreateBackupPhraseRouter()
		let interactor = CreateBackupPhraseInteractor(
			creator: BackupPhraseCreator(),
			presenter: CreateBackupPhrasePresenter(display: self)
		)
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		backupPhraseView.collectionViewLayout = BackupPhraseViewLayout()
    }
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		backupPhraseView.collectionViewLayout.invalidateLayout()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		UIApplication.willEnterForegroundNotification.addObserver(self, selector: #selector(refreshBackupPhrase))

		refreshBackupPhrase()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

		UIApplication.willEnterForegroundNotification.removeObserver(self)
	}
	
	@IBAction func refreshBackupPhrase() {
		interactor?.execute(.init())
	}
}

extension CreateBackupPhraseViewController: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return words.count
	}
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "word", for: indexPath)
		if let cell = cell as? WordItemView {
			cell.indexView.text = "\(indexPath.row + 1)"
			cell.valueView.text = words[indexPath.row]
		}
		return cell
	}
}

extension CreateBackupPhraseViewController: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return sizes[indexPath.row]
	}
}

extension CreateBackupPhraseViewController: CreateBackupPhraseDisplayLogic {
	func show(_ viewModel: Wallet.BackupPhrase.ViewModel) {
		let valueView = UILabel()
		valueView.font = .systemFont(ofSize: 17)
		
		words = viewModel.words
		sizes = words.enumerated().map {
			valueView.text = $1
			valueView.sizeToFit()
			return CGSize(width: 32 + valueView.frame.width + 12, height: 32)
		}

		if isViewLoaded {
			backupPhraseView.reloadData()
			backupPhraseView.alpha = 0
			UIView.animate(withDuration: -1) {
				self.backupPhraseView.alpha = 1
			}
		}
	}
}

final class WordItemView: UICollectionViewCell {
	@IBOutlet weak var indexView: UILabel!
	@IBOutlet weak var valueView: UILabel!
}

final class BackupPhraseView: UICollectionView {
	override var intrinsicContentSize: CGSize {
		return collectionViewLayout.collectionViewContentSize
	}
}
