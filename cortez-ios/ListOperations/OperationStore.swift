// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import CoreData

extension Operation: Storable {

	enum Predicate {
		case all
		case hashEquals(String)
		case hashIn([String])
		case sourceOrDestinationEquals(Address)
		case sourceOrDestinationIn([Address])
	}
	
	enum Attribute: String {
		case indexTitle
		case timestamp
		case operationHash
	}
}

extension Operation.Attribute: Named {
	var name: String { rawValue }
}

typealias OperationStoringLogic = AnyStore<Operation>

extension OperationDTO {
	static func fetchRequest(with predicate: NSPredicate?) -> NSFetchRequest<OperationDTO> {
		let request: NSFetchRequest<OperationDTO> = fetchRequest()
		request.predicate = predicate
		return request
	}
}

extension Operation: CoreDataBacked {
	
	var request: NSFetchRequest<OperationDTO> {
		return Predicate.hashEquals(operationHash).request
	}
	
	init(from dto: OperationDTO) {
		self.init(
			sourceName: dto.sourceName,
			destinationName: dto.destinationName,
			operationHash: dto.operationHash ?? "",
			id: Int(dto.id),
			blockHash: dto.blockHash ?? "",
			level: Int(dto.level),
			timestamp: dto.timestamp ?? Date(),
			source: dto.source ?? "",
			sourceManager: dto.sourceManager,
			destination: dto.destination ?? "",
			destinationManager: dto.destinationManager,
			amount: UInt64(dto.amount),
			fee: UInt64(dto.fee)
		)
	}
	
	func encode(to dto: OperationDTO) {
		dto.sourceName = sourceName
		dto.destinationName = destinationName
		dto.operationHash = operationHash
		dto.id = Int64(id)
		dto.blockHash = blockHash
		dto.level = Int64(level)
		dto.timestamp = timestamp
		dto.source = source
		dto.sourceManager = sourceManager
		dto.destination = destination
		dto.destinationManager = destinationManager
		dto.amount = Int64(amount)
		dto.fee = Int64(fee)
		
		let calendar = Calendar.current
		let components = calendar.dateComponents([.year, .month, .day], from: timestamp)
		dto.indexTitle = [
			String(components.year ?? 0).prefixing(toLength: 4, withPrefix: "0", startingAt: 0),
			String(components.month ?? 0).prefixing(toLength: 2, withPrefix: "0", startingAt: 0),
			String(components.day ?? 0).prefixing(toLength: 2, withPrefix: "0", startingAt: 0)
		].joined(separator: "")
	}
}

extension Operation.Predicate: FetchRequestConvertible {
	var request: NSFetchRequest<OperationDTO> {
		let request: NSFetchRequest<OperationDTO> = OperationDTO.fetchRequest()
		switch self {
		case .all:
			break
		case let .hashEquals(hash):
			request.predicate = NSPredicate(format: "operationHash = %@", hash)
		case let .hashIn(hashes):
			request.predicate = NSPredicate(format: "operationHash IN %@", hashes)
		case let .sourceOrDestinationEquals(address):
			request.predicate = NSPredicate(format: "source = %@ || destination = %@", address, address)
		case let .sourceOrDestinationIn(addresses):
			request.predicate = NSPredicate(format: "source IN %@ || destination IN %@", addresses, addresses)
		}
		
		return request
	}
}
