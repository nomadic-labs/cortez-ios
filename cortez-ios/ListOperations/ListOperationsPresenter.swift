// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ListOperationsPresentationLogic {
	func format(_ response: History.Response)
	func format(_ response: History.Account.Response)
	func format(_ response: History.Operation.List.Response)
}

struct ListOperationsPresenter: ListOperationsPresentationLogic {
	
	let balanceFormatter: NumberFormatter = .tez()

	let dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateStyle = .long
		formatter.timeStyle = .none
		formatter.doesRelativeDateFormatting = true
		return formatter
	}()
	let amountFormatter: NumberFormatter = .tez()

	weak var display: ListOperationsDisplayLogic?
	
	func format(_ response: History.Response) {
		guard response.error == nil else {
			display?.show(.init(message: "Something went wrong with your request."))
			return
		}
		display?.show(.init(message: nil))
	}

	func format(_ response: History.Account.Response) {
		display?.show(
			.init(
				name: response.name,
				balance: formatBalance(response.balance),
				delegate: response.delegate.map { $0.isEmpty ? "none" : $0 }
			)
		)
	}

	private func formatBalance(_ balance: UInt64?) -> NSAttributedString? {
		guard let balance = balance else { return nil }
		guard let formattedBalance = balanceFormatter.string(for: Double(balance).tez) else { return nil } // TODO: error
		guard let cortezFont = UIFont(name: "cortez", size: 21) else { return nil }
		return NSAttributedString(string: formattedBalance, attributes: [.font: cortezFont])
	}

	func format(_ response: History.Operation.List.Response) {
		let dateFormatter = self.dateFormatter
		let amountFormatter = self.amountFormatter
		display?.show(.init(selection: response.selection?.map {
			let date = $0.row == 0 ? dateFormatter.string(for: $1.timestamp) : nil
			return .init(from: $1, with: date, formatter: amountFormatter)
		}))
	}
}

extension History.Operation.List.ViewModel.Row {
	init(from operation: Operation, with date: String?, formatter: NumberFormatter) {
		self.date = date
		self.source = operation.sourceName ?? operation.source
		self.destination = operation.destinationName ?? operation.destination
		self.amount = formatter.string(for: Double(operation.amount).tez)
	}
}
