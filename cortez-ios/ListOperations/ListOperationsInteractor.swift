// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation
import KeychainAccess

protocol ListOperationsDataStore {
	var isDelegatable: Bool { get set }
	var accountQuery: Query<Account>? { get set }
	var operationQuery: Query<Operation>? { get set }
	var selectedOperation: Operation? { get set }
}

protocol ListOperationsBusinessLogic {
	
	var isDelegatable: Bool { get }
	
	func selectOperation(at indexPath: IndexPath)
	func execute(_ request: History.Request)
}

final class ListOperationsInteractor: ListOperationsBusinessLogic, ListOperationsDataStore {

	var isDelegatable: Bool = false
	
	var accountQuery: Query<Account>? {
		didSet {
			reloadAccountSelection()
		}
	}
	var operationQuery: Query<Operation>? {
		didSet {
			reloadOperationSelection()
		}
	}
	
	var accountSelection: AnySelection<Account>?
	var operationSelection: AnySelection<Operation>?

	var selectedOperation: Operation?

	let contractsRetriever: ContractsRetrievingLogic
	let historyRetriever: HistoryRetrievingLogic
	
	let presenter: ListOperationsPresentationLogic

	init(contractsRetriever: ContractsRetrievingLogic, historyRetriever: HistoryRetrievingLogic, presenter: ListOperationsPresentationLogic) {
		self.contractsRetriever = contractsRetriever
		self.historyRetriever = historyRetriever
		self.presenter = presenter

		reloadAccountSelection()
		reloadOperationSelection()
	}
	
	func selectOperation(at indexPath: IndexPath) {
		selectedOperation = operationSelection?[indexPath]
	}

	func execute(_ request: History.Request) {
		// TODO: get accounts
		guard let manager = Source.manager?.address else { return }
		contractsRetriever.contracts(for: manager) { [weak self] result in
			guard let `self` = self else { return }
			defer { self.presenter.format(.init(error: result.error)) }
			guard case let .success(contracts) = result else { return }
			self.updateStore(with: contracts)
			
			self.retrieveHistory()
		}
	}
	
	private func retrieveHistory() {
		guard let addresses = accountSelection?.reduce([], { $0 + $1.map { $0.address } }) else { return }
		historyRetriever.history(for: addresses) { [weak self] result in
			//defer { group.leave() }
			guard let `self` = self else { return }
			switch result {
			case let .success(operations):
				// TODO: merger
				guard let store: AnyStore<Operation> = Provider.shared.get() else { return }
				store.enqueue { context in
					let query = Query<Operation>.where(.hashIn(operations.map { $0.operationHash }))
						.sorted(by: .operationHash, ascending: false)
					let alreadyStoredOperations = try context.select(query)
					try Set(operations).subtracting(alreadyStoredOperations).forEach { try context.insert($0) }
				}
				self.presenter.format(.init(error: nil))
			case let .failure(error):
				self.presenter.format(.init(error: error))
			}
		}
	}
	
	private func reloadAccountSelection() {
		guard let query = accountQuery else {
			clearOperationSelection()
			return
		}

		guard let accounts: AnyStore<Account> = Provider.shared.get() else { return }
		var accountSelection = accounts.select(query.sorted(by: .index, ascending: true))
		self.accountSelection = accountSelection
		accountSelection.delegate = AnySelectionDelegate(self)
		presentBalanceAndDelegate()
	}
	
	private func presentBalanceAndDelegate() {
		guard let accountSelection = accountSelection else {
			return presenter.format(.init(name: nil, balance: nil, delegate: nil))
		}
		let (name, delegate): (String?, String?) = {
			guard case .addressEquals = accountQuery?.predicate else { return (nil, nil) }
			guard let account = accountSelection.first?.first else { return (nil , nil) }
			guard let store: AnyStore<Contact> = Provider.shared.get() else { return (nil, nil) }
			let name = accountSelection.first?.first.map { $0.name ?? $0.address }
			if let delegate = account.delegate, let contact = store.select(.where(.addressEquals(delegate))).first?.first {
				return (name, contact.name)
			}
			return (name, account.delegate ?? "")
		}()
		let balances = accountSelection.reduce([], { $0 + $1.compactMap { $0.balance } })
		presenter.format(
			.init(
				name: name,
				balance: balances.isEmpty ? nil : balances.reduce(0, +),
				delegate: delegate
			)
		)
	}
	
	private func clearAccountSelection() {
		accountSelection?.delegate = nil
		accountSelection = nil
		// TODO: call presenter
	}
	
	private func reloadOperationSelection() {
		guard let query = operationQuery else {
			clearOperationSelection()
			return
		}

		guard let operations: AnyStore<Operation> = Provider.shared.get() else { return }
		let operationSelection = operations.select(
			query.sorted(by: .indexTitle, ascending: false).sorted(by: .timestamp, ascending: false)
		)
		self.operationSelection = operationSelection
		presenter.format(.init(selection: operationSelection))
	}
	
	private func clearOperationSelection() {
		operationSelection?.delegate = nil
		operationSelection = nil
		presenter.format(.init(selection: nil))
	}
	
	private func updateStore(with contracts: [Contract]) {
		let addresses: [String] = contracts.map {
			[$0.address, $0.operationHash].compactMap { $0 }
		}.flatMap { $0 }
		guard let store: AnyStore<Account> = Provider.shared.get() else { return }
		store.enqueue { context in
			let query = Query<Account>.where(.addressIn(addresses)).sorted(by: .index, ascending: true)
			let accounts = try context.select(query).reduce(into: [:]) { $0[$1.address] = $1 }
			try contracts
				.sorted { lhs, _ in !lhs.address.isOriginated }
				.enumerated()
				.forEach { try self.updateContext(context, with: $1, at: $0, using: accounts) }
		}
	}
	
	private func updateContext<T: Context>(_ context: T, with contract: Contract, at index: Int, using accounts: [Address: Account]) throws where Account == T.Element {
		if var account = accounts[contract.address] { // update
			account.index = Int64(index)
			account.balance = contract.balance
			account.delegate = contract.delegate
			try context.update(account)
		} else if let operationHash = contract.operationHash, var account = accounts[operationHash] { // update & resolve name
			try context.delete(account)
			account.index = Int64(index)
			account.address = contract.address
			account.balance = contract.balance
			account.delegate = contract.delegate
			try context.insert(account)
			guard let name = account.name else { return }
			guard let store: AnyStore<Contact> = Provider.shared.get() else { return }
			store.insert(Contact(name: name, address: account.address))
		} else { // insert
			let account = Account(
				index: Int64(index),
				address: contract.address,
				source: Source.manager?.address,
				balance: contract.balance,
				delegate: contract.delegate
			)
			try context.insert(account)
		}
	}
}

extension ListOperationsInteractor: SelectionDelegate {
	func selection<T: Selection>(_ selection: T, didUpdate account: Account, at indexPath: IndexPath) where Account == T.SectionElement {
		presentBalanceAndDelegate()
	}
}
