// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ShowOperationDisplayLogic: class {
	func show(_ viewModel: History.Operation.Detail.ViewModel)
}

class ShowOperationViewController: UIViewController {

	var router: (ShowOperationRoutingLogic & ShowOperationDataPassingLogic)?
	var interactor: (ShowOperationBusinessLogic & ShowOperationDataStore)?
	
	@IBOutlet private weak var navigationBar: UINavigationBar!
	@IBOutlet weak var propertyListView: UITableView!
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}

	var rows: [History.Operation.Detail.ViewModel.Row] = []

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = ShowOperationRouter()
		let interactor = ShowOperationInteractor(presenter: ShowOperationPresenter(display: self))
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		navigationBar.isHidden = navigationController != nil
		navigationItem.title = navigationBar.topItem?.title
		interactor?.execute(.init())
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
	
	@IBAction func unwind(segue: UIStoryboardSegue) {
		// nothing to do
	}
}

extension ShowOperationViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return rows.count
	}
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let row = rows[indexPath.row]
		let cell = tableView.dequeueReusableCell(withIdentifier: row.cell.rawValue, for: indexPath)
		var maskedCorners: CACornerMask = []
		if indexPath.row == 0 {
			maskedCorners.formUnion([.layerMinXMinYCorner, .layerMaxXMinYCorner])
		}
		if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
			maskedCorners.formUnion([.layerMinXMaxYCorner, .layerMaxXMaxYCorner])
		}
		cell.layer.cornerRadius = 6
		cell.layer.maskedCorners = maskedCorners
		if var cell = cell as? OperationPropertyRow {
			cell.title = row.key
			cell.value = row.value
			if case .address = row.cell {
				cell.action = { [weak self] in
					let sheet = UIAlertController(title: row.value, message: nil, preferredStyle: .actionSheet)
					sheet.addAction(UIAlertAction(title: "Add to Contacts", style: .default) { _ in
						self?.interactor?.selectedAddress = row.value
						self?.router?.routeToCreateContact(with: nil)
					})
					sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
					self?.present(sheet, animated: true, completion: nil)
				}
			}
		}
		return cell
	}
}

extension ShowOperationViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return CGFloat.leastNormalMagnitude
	}
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return nil
	}
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
	}
}

extension ShowOperationViewController: ShowOperationDisplayLogic {
	func show(_ viewModel: History.Operation.Detail.ViewModel) {
		rows = viewModel.rows
		propertyListView.reloadData()
	}
}
