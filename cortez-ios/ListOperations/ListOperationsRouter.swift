// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ListOperationsRoutingLogic {
	func routeToAccountEdition(with segue: UIStoryboardSegue?)
	func routeToSelectedOperation(with segue: UIStoryboardSegue?)
}

protocol ListOperationsDataPassingLogic {
	var dataStore: ListOperationsDataStore? { get }
}

final class ListOperationsRouter: ListOperationsRoutingLogic, ListOperationsDataPassingLogic {
	
	var dataStore: ListOperationsDataStore?
	
	weak var viewController: ListOperationsViewController?
	
	func routeToAccountEdition(with segue: UIStoryboardSegue?) {
		guard let sourceDataStore = dataStore else { return }
		guard let onboardController = segue?.destination as? OnboardController else { return }
		onboardController.router = EditAccountRouter()
		guard var destinationDataStore = onboardController.router?.dataStore as? EditAccountDataStore else { return }
		passData(from: sourceDataStore, to: &destinationDataStore)
	}
	func routeToSelectedOperation(with segue: UIStoryboardSegue?) {
		guard let segue = segue else { return }
		guard let dataStore = dataStore else { return }
		guard let destination = segue.destination as? ShowOperationViewController else { return }
		guard var destinationDataSource = destination.router?.dataStore else { return }
		passData(from: dataStore, to: &destinationDataSource)
	}

	func passData(from source: ListOperationsDataStore, to destination: inout EditAccountDataStore) {
		guard let query = source.accountQuery else { return }
		guard let accounts: AnyStore<Account> = Provider.shared.get() else { return }
		
		let selection = accounts.select(query.sorted(by: .index, ascending: true))
		guard let account = selection.first?.first else { return }
		destination.account = account

		if let delegate = account.delegate { // find delegate contact if any
			guard let contacts: AnyStore<Contact> = Provider.shared.get() else { return }
			let selection = contacts.select(Query.where(.addressEquals(delegate)).sorted(by: .name, ascending: true))
			guard let contact = selection.first?.first else { return }
			destination.delegate = contact
		}
	}
	func passData(from source: ListOperationsDataStore, to destination: inout ShowOperationDataStore) {
		destination.operation = source.selectedOperation
	}
}
