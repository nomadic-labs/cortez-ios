// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

protocol HistoryRetrievingLogic {
	
	typealias Completion = (Result<[Operation], Call.Error>) -> Void

	func history<T: Sequence>(for addresses: T, completion: @escaping Completion) where Address == T.Element
}

extension HistoryRetrievingLogic {
	func history(for address: Address, completion: @escaping Completion) {
		history(for: CollectionOfOne(address), completion: completion)
	}
}

final class HistoryRetriever: HistoryRetrievingLogic {
	
	private var call: Call?

	func history<T: Sequence>(for addresses: T, completion: @escaping Completion) where Address == T.Element {
		call = Call(remoteProcedure: .history(for: addresses), session: Provider.shared.get() ?? .shared)
		call?.enqueue(
			jsonDecoder: JSONDecoder(dateDecodingStrategy: .iso8601),
			onValue: { (operations: [[Operation]]) in completion(.success(operations.flatMap { $0 })) },
			onError: { error in completion(.failure(error)) }
		)
	}
}
