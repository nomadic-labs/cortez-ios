// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit
import LocalAuthentication
import KeychainAccess

class ListSettingsDetailsController: UITableViewController {

	@IBOutlet var isCredentialsNeededWhenAppBecomesActiveSwitch: UISwitch!
	@IBOutlet var isFingerprintAllowedForTransfersSwitch: UISwitch!
	
	private let queue = DispatchQueue(
		label: "settings",
		qos: .userInteractive,
		attributes: [],
		autoreleaseFrequency: .inherit,
		target: nil
	)

    override func viewDidLoad() {
        super.viewDidLoad()

		let settings = Settings.shared
		isCredentialsNeededWhenAppBecomesActiveSwitch.isOn = settings.isCredentialsNeededWhenAppBecomesActive
		isFingerprintAllowedForTransfersSwitch.isOn = settings.isFingerprintAllowedForTransfers
    }
	
	@IBAction func toggleIsCredentialsNeededWhenAppBecomesActive(sender: UISwitch) {
		let settings = Settings.shared
		guard sender.isOn else {
			settings.isCredentialsNeededWhenAppBecomesActive = false
			return
		}
		var error: NSError?
		if LAContext().canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
			settings.isCredentialsNeededWhenAppBecomesActive = true
			return
		}
		if let error = error {
			showErrorMessage(error.localizedDescription)
			sender.setOn(false, animated: true)
		}
	}
	
	@IBAction func toogleIsFingerprintAllowedForTransfers(sender: UISwitch) {
		guard sender.isOn else {
			try? Keychain().remove("biometry-secret-key")
			Settings.shared.isFingerprintAllowedForTransfers = false
			return
		}
		queue.async {
			var keychain = Keychain()
			do {
				if let secretKey: PrivateKey = try keychain.getData("secret-key") {
					keychain = keychain.accessibility(.whenUnlockedThisDeviceOnly, authenticationPolicy: .userPresence)
					try? keychain.set(secretKey, key: "biometry-secret-key")
					DispatchQueue.main.async { Settings.shared.isFingerprintAllowedForTransfers = true }
				}
			} catch {
				DispatchQueue.main.async { sender.setOn(false, animated: true) }
			}
		}
	}
	
	@IBAction func exportSeedPhrase() {
		queue.async {
			guard let seedPhrase = try? Keychain().get("backup-phrase") else { return }
			DispatchQueue.main.async {
				let activityViewController = UIActivityViewController(activityItems: [seedPhrase], applicationActivities: nil)
				self.present(activityViewController, animated: true, completion: nil)
			}
		}
	}
	
	private func showErrorMessage(_ message: String) {
		let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
		present(alert, animated: true, completion: nil)
	}

    // MARK: - Table view delegate
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		UITableView.automaticDimension
	}

	override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		UITableView.automaticDimension
	}
}
