// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ListAccountsDisplayLogic: class {
	func show(_ viewModel: Account.List.Reload.ViewModel)
	func show(_ viewModel: Account.List.ViewModel)
}

class ListAccountsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, OnboardPageViewController {

	var router: (OnboardPageDataPassingLogic & OnboardPageRoutingLogic)?
	var interactor: ListAccountsBusinessLogic?

	var selection: AnySelection<Account.List.Item>?

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = ListAccountsRouter()
		let interactor = ListAccountsInteractor(
			contractsRetriever: ContractsRetriever(),
			presenter: ListAccountsPresenter(display: self)
		)
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Do any additional setup after loading the view.
		let refreshControl = UIRefreshControl()
		refreshControl.tintColor = .white
		refreshControl.addTarget(self, action: #selector(load), for: .valueChanged)
		collectionView.refreshControl = refreshControl
		collectionView.contentOffset = CGPoint(x: 0, y: -refreshControl.frame.size.height)
		collectionView.contentOffset = .zero

		setSelectionDelegate()
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		onboardController?.isNextButtonHidden = true
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		load()
	}

	@objc func load() {
		interactor?.execute(.init())
	}
	
	private func setSelectionDelegate() {
		selection?.delegate = AnySelectionDelegate(SelectionCollectionViewControl(collectionView: collectionView))
	}

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
		(router as? ListAccountsRoutingLogic)?.routeToCreateAccount(with: segue)
    }
	
	@IBAction func unwindToListAccounts(segue: UIStoryboardSegue) {
		// nothing to do
	}
	@IBAction func unwind(segue: UIStoryboardSegue) {
		// nothing to do
	}

    // MARK: UICollectionViewDataSource
	
	override func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1//selection?.count ?? 1
	}

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
		return (selection?[section].count ?? 0) + (section == 0 ? 1 : 0)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if indexPath.section == 0 && indexPath.item == (selection?[indexPath.section].count ?? 0) {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "create", for: indexPath)
			return cell
		}
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "account", for: indexPath)
		if let item = selection?[indexPath], let cell = cell as? AccountItemView {
			cell.nameView.text = item.name
			cell.balanceView.text = item.balance
			if item.balance == nil {
				cell.spinnerView.startAnimating()
			}
		}
        return cell
    }

    // MARK: UICollectionViewDelegate
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let width = (collectionView.bounds.width - 16 * 3) / 2
		return CGSize(width: width, height: width + 49)
	}
	
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let selection = selection else { return }
		guard let indexPath = collectionView.indexPathsForSelectedItems?.first else { return }
		guard indexPath.section < selection.count && indexPath.item < selection[indexPath.section].count else {
			return
		}
		interactor?.selectAccount(at: indexPath)
		router?.routeToNextPage(with: nil)
	}
}

extension ListAccountsViewController: ListAccountsDisplayLogic {
	func show(_ viewModel: Account.List.Reload.ViewModel) {
		if let refreshControl = collectionView.refreshControl {
			refreshControl.endRefreshing()
		}
		guard let message = viewModel.message else { return }
		let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		let retryButton = UIAlertAction(title: "Retry", style: .default) { _ in
			self.load()
		}
		let okButton = UIAlertAction(title: "Ok", style: .default) { _ in }
		alert.addAction(retryButton)
		alert.addAction(okButton)
		present(alert, animated: true, completion: nil)
	}

	func show(_ viewModel: Account.List.ViewModel) {
		selection = viewModel.selection
		if isViewLoaded {
			setSelectionDelegate()
			collectionView.reloadData()
		}
	}
}

final class AccountItemView: UICollectionViewCell {
	@IBOutlet var nameView: UILabel!
	@IBOutlet var balanceView: UILabel!
	@IBOutlet var spinnerView: UIActivityIndicatorView!
	
	var control: RippleTouchControl?

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setRippleControl()
	}
	
	func setRippleControl() {
		control = RippleTouchControl(with: self)
		if let color = UIColor(named: "text")?.withAlphaComponent(0.5) {
			control?.rippleView?.rippleColor = color
		}
	}
}

final class AddNewSpaceItemView: UICollectionViewCell {
	
	var control: RippleTouchControl?

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setRippleControl()
	}
	
	func setRippleControl() {
		control = RippleTouchControl(with: self)
		if let color = UIColor(named: "text")?.withAlphaComponent(0.5) {
			control?.rippleView?.rippleColor = color
		}
	}
}
