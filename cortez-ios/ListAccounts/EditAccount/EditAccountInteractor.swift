//
//  EditAccountInteractor.swift
//  cortez-ios
//
//  Created by pikatos on 06/12/2019.
//  Copyright © 2019 Steve Sanches. All rights reserved.
//

import Foundation

protocol DelegateDataStore {
	var delegate: Contact? { get set }
}

protocol EditAccountDataStore: OnboardDataStore, DelegateDataStore {
	var account: Account? { get set }
}

final class EditAccountInteractor: EditAccountDataStore {
	var account: Account?
	var delegate: Contact?
}
