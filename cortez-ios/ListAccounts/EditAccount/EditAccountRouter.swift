//
//  EditAccountRouter.swift
//  cortez-ios
//
//  Created by pikatos on 06/12/2019.
//  Copyright © 2019 Steve Sanches. All rights reserved.
//

import UIKit

final class EditAccountRouter: OnboardRouter {

	convenience init() {
		let pageViewControllers: [OnboardPageViewController] = [
			("CreateAccount", "delegate-account", Optional<String>.none),
			("Transfer", "confirm-payment", nil)
		].compactMap {
			let storyboard = UIStoryboard(name: $0, bundle: nil)
			let pageViewController = storyboard.instantiateViewController(withIdentifier: $1) as? OnboardPageViewController
			pageViewController?.title = $2
			return pageViewController
		}
		self.init(pageViewControllers)
		dataStore = EditAccountInteractor()
	}

	override func passData(from source: OnboardDataStore, to destination: inout OnboardPageDataStore) {
		guard let source = source as? EditAccountDataStore else { return }
		switch destination {
		case var destination as DelegateAccountDataStore:
			passData(from: source, to: &destination)
		case var destination as ConfirmPaymentDataStore:
			passData(from: source, to: &destination)
		default:
			break
		}
	}
	func passData(from source: EditAccountDataStore, to destination: inout DelegateAccountDataStore) {
		destination.delegate = source.delegate
		destination.selectedDelegate = source.delegate
		if source.delegate != nil {
			destination.isDelegationEnabled = true
		}
	}
	func passData(from source: EditAccountDataStore, to destination: inout ConfirmPaymentDataStore) {
		guard let target = source.account else { return }
		destination.transfer = .delegation(
			Transfer.Delegation(source: target, delegate: source.delegate)
		)
	}
}
