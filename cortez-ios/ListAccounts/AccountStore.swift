// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation
import CoreData

struct Account {
	
	var index: Int64
	var name: String?
	var address: Address
	var source: Address?
	var balance: UInt64?
	var delegate: Address?

	init(index: Int64, name: String? = nil, address: Address, source: Address? = nil, balance: UInt64? = nil, delegate: Address? = nil) {
		self.index = index
		self.name = name
		self.address = address
		self.source = address == source ? nil : source // sanity check: main account should not have a source
		self.balance = balance
		self.delegate = delegate
	}
}

extension Account: Storable {

	enum Predicate {
		case all
		case addressEquals(Address)
		case addressIn([Address])
		case addressOrSourceEquals(Address)
		case pendingAccounts
		case mainAccount
	}
	
	enum Attribute: String {
		case index
	}
}

extension Account.Attribute: Named {
	var name: String { rawValue }
}

extension Account: Hashable {
	static func ==(lhs: Account, rhs: Account) -> Bool {
		return lhs.address == rhs.address
	}
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(address)
	}
}

typealias AccountStoringLogic = AnyStore<Contact>

extension AccountDTO {
	static func fetchRequest(with predicate: NSPredicate?) -> NSFetchRequest<AccountDTO> {
		let request: NSFetchRequest<AccountDTO> = fetchRequest()
		request.predicate = predicate
		return request
	}
}

extension Account: CoreDataBacked {
	
	var request: NSFetchRequest<AccountDTO> {
		return Predicate.addressEquals(address).request
	}
	
	init(from dto: AccountDTO) {
		
		self.init(
			index: dto.index,
			name: dto.name,
			address: dto.address ?? "",
			source: dto.source,
			balance: dto.balance?.uint64Value,
			delegate: dto.delegate
		)
	}
	
	func encode(to dto: AccountDTO) {
		dto.index = index
		dto.name = name
		dto.address = address
		dto.source = source
		dto.balance = balance.map(NSNumber.init)
		dto.delegate = delegate
	}
}

extension Account.Predicate: FetchRequestConvertible {
	var request: NSFetchRequest<AccountDTO> {
		let request: NSFetchRequest<AccountDTO> = AccountDTO.fetchRequest()
		switch self {
		case .all:
			request.predicate = NSPredicate(format: "index != %i", -1)
		case let .addressEquals(address):
			request.predicate = NSPredicate(format: "address = %@", address)
		case let .addressIn(addresses):
			request.predicate = NSPredicate(format: "address IN %@", addresses)
		case let .addressOrSourceEquals(address):
			request.predicate = NSPredicate(format: "(address = %@ || source = %@) && index != %i", address, address, -1)
		case .pendingAccounts:
			request.predicate = NSPredicate(format: "index = %i", -1)
		case .mainAccount:
			request.predicate = NSPredicate(format: "index = %i", 0)
		}
		return request
	}
}
