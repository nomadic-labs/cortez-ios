// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

typealias OperationHash = String

struct Contract {
	var balance: UInt64
	var address: Address
	var operationHash: OperationHash?
	var delegate: Address?
}

extension Contract: Decodable {
	
	enum Error: Swift.Error {
		case invalidBalance
	}
	enum CodingKeys: String, CodingKey {
		case balance
		case address = "k"
		case operationHash = "operation_hash"
		case delegate
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		balance = try UInt64(try container.decode(String.self, forKey: .balance)) ?? { throw Error.invalidBalance }()
		address = try container.decode(Address.self, forKey: .address)
		operationHash = try container.decodeIfPresent(OperationHash.self, forKey: .operationHash)
		delegate = try container.decodeIfPresent(Address.self, forKey: .delegate)
	}
}

protocol ContractsRetrievingLogic {
	
	typealias Completion = (Result<[Contract], Call.Error>) -> Void

	func contracts(for address: Address, completion: @escaping Completion)
}

final class ContractsRetriever: ContractsRetrievingLogic {

	private var call: Call?

	func contracts(for address: Address, completion: @escaping Completion) {
		call = Call(remoteProcedure: .contracts(for: address), session: Provider.shared.get() ?? .shared)
		call?.enqueue(
			onValue: { (contracts: [[Contract]]) in completion(.success(contracts.first ?? [])) },
			onError: { error in completion(.failure(error)) }
		)
	}
}
