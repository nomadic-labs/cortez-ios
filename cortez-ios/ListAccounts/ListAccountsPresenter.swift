// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

protocol ListAccountsPresentationLogic {
	func format(_ response: Account.List.Reload.Response)
	func format(_ response: Account.List.Response)
}

struct ListAccountsPresenter: ListAccountsPresentationLogic {

	let balanceFormatter: NumberFormatter = .tez()

	weak var display: ListAccountsDisplayLogic?

	func format(_ response: Account.List.Reload.Response) {
		guard response.error == nil else {
			display?.show(.init(message: "Something went wrong with your request."))
			return
		}
		display?.show(.init(message: nil))
	}

	func format(_ response: Account.List.Response) {
		let viewModel: Account.List.ViewModel = .init(selection: response.selection?.map { _, account in
			let formattedBalance = account.balance
				.map(Int64.init) // bug with unsigned types?
				.map(Double.init)
				.map { $0.tez }
				.flatMap(self.balanceFormatter.string)
			return .init(
				name: account.name ?? account.address,
				balance: formattedBalance
			)
		})
		display?.show(viewModel)
	}
}
