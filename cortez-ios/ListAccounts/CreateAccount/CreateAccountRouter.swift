// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************


import UIKit

final class CreateAccountRouter: OnboardRouter {

	convenience init() {
		let pageViewControllers: [OnboardPageViewController] = [
			("CreateAccount", "name-account", Optional<String>.none),
			("CreateAccount", "credit-account", nil),
			("CreateAccount", "delegate-account", nil),
			("Transfer", "confirm-payment", nil)
		].compactMap {
			let storyboard = UIStoryboard(name: $0, bundle: nil)
			let pageViewController = storyboard.instantiateViewController(withIdentifier: $1) as? OnboardPageViewController
			pageViewController?.title = $2
			return pageViewController
		}
		self.init(pageViewControllers)
		dataStore = CreateAccountInteractor()
	}

	override func passData(from source: OnboardDataStore, to destination: inout OnboardPageDataStore) {
		guard let source = source as? CreateAccountDataStore else { return }
		switch destination {
		case var destination as NameAccountDataStore:
			passData(from: source, to: &destination)
		case var destination as CreditAccountDataStore:
			passData(from: source, to: &destination)
		case var destination as DelegateAccountDataStore:
			passData(from: source, to: &destination)
		case var destination as ConfirmPaymentDataStore:
			passData(from: source, to: &destination)
		default:
			break
		}
	}
	func passData(from source: CreateAccountDataStore, to destination: inout NameAccountDataStore) {
		destination.name = source.name
	}
	func passData(from source: CreateAccountDataStore, to destination: inout CreditAccountDataStore) {
		destination.amount = source.amount
	}
	func passData(from source: CreateAccountDataStore, to destination: inout DelegateAccountDataStore) {
		destination.delegate = Contact(name: "dummy", address: "dummy")
		destination.selectedDelegate = source.delegate
	}
	func passData(from source: CreateAccountDataStore, to destination: inout ConfirmPaymentDataStore) {
		guard let name = source.name else { return }
		guard let amount = source.amount else { return }
		destination.transfer = .origination(
			Transfer.Origination(name: name, amount: amount, delegate: source.delegate)
		)
	}
}
