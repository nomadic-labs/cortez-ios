// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol DelegateAccountDisplayLogic: class {
	func show(_ viewModel: Account.Delegate.ViewModel)
}

class DelegateAccountViewController: UITableViewController, OnboardPageViewController {
	
	var router: (OnboardPageRoutingLogic & OnboardPageDataPassingLogic)?
	var interactor: DelegateAccountBusinessLogic?

	@IBOutlet weak var delegateView: UITextField!
	@IBOutlet weak var delegateSwitch: UISwitch!

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = DelegateAccountRouter()
		let interactor = DelegateAccountInteractor(
			presenter: DelegateAccountPresenter(display: self)
		)
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		interactor?.perform(.init())
	}

	@IBAction func toggleDelegation(_ sender: UISwitch) {
		interactor?.isDelegationEnabled = sender.isOn
		if sender.isOn {
			(router as? DelegateAccountRoutingLogic)?.routeToSelectDelegate(with: nil)
		}
	}
	

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

	@IBAction func unwind(segue: UIStoryboardSegue) {
	}
}

extension DelegateAccountViewController: UITextFieldDelegate {
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		(router as? DelegateAccountRoutingLogic)?.routeToSelectDelegate(with: nil)
		return false
	}
}

extension DelegateAccountViewController: DelegateAccountDisplayLogic {
	func show(_ viewModel: Account.Delegate.ViewModel) {
		guard isViewLoaded else { return }
		delegateView.text = viewModel.delegate
		delegateSwitch.setOn(viewModel.isDelegationEnabled, animated: true)
		delegateView.alpha = delegateSwitch.isOn ? 1 : 0.3
		onboardController?.isNextButtonEnabled = viewModel.isNextButtonEnabled
	}
}
