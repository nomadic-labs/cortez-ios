// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

class CreditAccountViewController: UITableViewController, OnboardPageViewController {

	var router: (OnboardPageRoutingLogic & OnboardPageDataPassingLogic)?
	var interactor: CreditAccountBusinessLogic?

	@IBOutlet weak var amountView: ValidableTextField!

	@IBOutlet weak var tezPadControl: TezPadControl!
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = CreditAccountRouter()
		let interactor = CreditAccountInteractor()
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}


    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
		update()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		update()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		// UIPageViewController scroll view issue during page transition
		UIView.animate(withDuration: -1, animations: {}) { _ in
			self.amountView.becomeFirstResponder()
		}
	}
	
	private func confirm() {
		performSegue(withIdentifier: "confirm", sender: self)
	}

	private func delegate() {
	}
	
	@IBAction func update() {
		amountView.text = tezPadControl.formattedString
		interactor?.setAccount(credit: tezPadControl.value)
	}

    // MARK: - Table view data source

    // MARK: - Navigation
	@IBAction func next() {
		router?.routeToNextPage(with: nil)
	}
}

extension CreditAccountViewController: TezPadControlDelegate {
	func tezPadControlDidUpdateAmount(_ tezPadControl: TezPadControl) {
		update()
	}
}
