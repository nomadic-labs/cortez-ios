// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import KeychainAccess

protocol ListAccountsBusinessLogic {
	func selectAccount(at indexPath: IndexPath)
	func execute(_ request: Account.List.Reload.Request)
}

protocol ListAccountsDataStore: OnboardPageDataStore {
	var address: Address? { get set }
	var selectedAccount: Account? { get set }
}

final class ListAccountsInteractor: ListAccountsBusinessLogic, ListAccountsDataStore {

	var address: Address? {
		didSet {
			reload()
		}
	}
	var selectedAccount: Account?

	var selection: AnySelection<Account>?

	let contractsRetriever: ContractsRetrievingLogic

	let presenter: ListAccountsPresentationLogic

	init(contractsRetriever: ContractsRetrievingLogic, presenter: ListAccountsPresentationLogic) {
		self.contractsRetriever = contractsRetriever
		self.presenter = presenter

		reload()
	}
	
	func selectAccount(at indexPath: IndexPath) {
		selectedAccount = selection?[indexPath]
	}

	func execute(_ request: Account.List.Reload.Request) {
		guard let address = address else { return }
		contractsRetriever.contracts(for: address) { [weak self] result in
			guard let `self` = self else { return }
			defer { self.presenter.format(.init(error: result.error)) }
			guard case let .success(contracts) = result else { return }
			self.updateStore(with: contracts)
		}
	}
	
	private func updateStore(with contracts: [Contract]) {
		let addresses: [String] = contracts.map {
			[$0.address, $0.operationHash].compactMap { $0 }
		}.flatMap { $0 }
		guard let store: AnyStore<Account> = Provider.shared.get() else { return }
		store.enqueue { context in
			let query = Query<Account>.where(.addressIn(addresses)).sorted(by: .index, ascending: true)
			let accounts = try context.select(query).reduce(into: [:]) { $0[$1.address] = $1 }
			try contracts
				.sorted { lhs, _ in !lhs.address.isOriginated }
				.enumerated()
				.forEach { try self.updateContext(context, with: $1, at: $0, using: accounts) }
		}
	}
		
	private func updateContext<T: Context>(_ context: T, with contract: Contract, at index: Int, using accounts: [Address: Account]) throws where Account == T.Element {
		if var account = accounts[contract.address] { // update
			account.index = Int64(index)
			account.balance = contract.balance
			account.delegate = contract.delegate
			try context.update(account)
		} else if let operationHash = contract.operationHash, var account = accounts[operationHash] { // update & resolve name
			try context.delete(account)
			account.index = Int64(index)
			account.address = contract.address
			account.balance = contract.balance
			account.delegate = contract.delegate
			try context.insert(account)
			guard let name = account.name else { return }
			guard let store: AnyStore<Contact> = Provider.shared.get() else { return }
			store.insert(Contact(name: name, address: account.address))
		} else { // insert
			let account = Account(
				index: Int64(index),
				address: contract.address,
				source: address,
				balance: contract.balance,
				delegate: contract.delegate
			)
			try context.insert(account)
		}
	}
	
	private func reload() {
		guard let address = address else {
			clearSelection()
			return
		}
		guard let accounts: AnyStore<Account> = Provider.shared.get() else { return }
		let selection = accounts.select(Query.where(.addressOrSourceEquals(address)).sorted(by: .index, ascending: true))
		self.selection = selection
		presenter.format(.init(selection: selection))
	}

	private func clearSelection() {
		selection?.delegate = nil
		selection = nil
		presenter.format(.init(selection: nil))
	}
}
