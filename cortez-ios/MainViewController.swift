// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit
import KeychainAccess

enum Cortez {
	static let userDidLogin = Notification.Name("userDidLogin")
	static let userDidLogout = Notification.Name("userDidLogout")
}

enum WalletSegue: String {
	case create
	case restore
}

extension UIViewController {
	static func dummyViewController(with color: UIColor) -> UIViewController {
		let viewController = UIViewController()
		viewController.view.backgroundColor = color
		return viewController
	}
}

class MainViewController: UIViewController {
	
	var router: (MainRoutingLogic & MainDataPassingLogic)?
	var interactor: MainBusinessLogic?

	var pageViewController: UIPageViewController! {
		return (children.first as! UIPageViewController)
	}

	var currentIndex: Int = 0
	var currentSpeed: Int = 1
	
	@IBOutlet weak var sectionSelectorView: UIStackView!
	@IBOutlet weak var sectionCursorOffset: NSLayoutConstraint!
	
	@IBOutlet weak var sectionCursor: UIView!
	@IBOutlet weak var lineSeparator: UIView!
	
	@IBOutlet weak var emptyView: UIView!

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = MainRouter()
		let interactor = MainInteractor()
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		pageViewController.dataSource = self
		pageViewController.delegate = self
		pageViewController.scrollView?.delegate = self
		pageViewController.setViewControllers(
			router?.sections.first.map { [$0] },
			direction: .forward,
			animated: false,
			completion: nil
		)

		sectionSelectorView.arrangedSubviews.forEach { $0.alpha = 0.5 }
		sectionSelectorView.arrangedSubviews[currentIndex].alpha = 1
		pageViewController.scrollView?.setContentOffset(.zero, animated: false)
		
		installEmptyView()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		showEmptyViewIfNeeded()
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		Cortez.userDidLogout.addObserver(self, selector: #selector(showEmptyViewIfNeeded))
	}
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		Cortez.userDidLogout.removeObserver(self)
	}

	@IBAction func selectSection(_ sender: UIButton) {
		guard let index = sectionSelectorView.arrangedSubviews.firstIndex(of: sender) else { return }
		selectSection(at: index)
	}
	
	private func selectSection(at index: Int) {
		if index == currentIndex {
			return
		}
		let direction: UIPageViewController.NavigationDirection = currentIndex < index ? .forward: .reverse
		let viewController = router?.sections[index]
		currentSpeed = abs(index - currentIndex)
		view.isUserInteractionEnabled = false
		pageViewController.setViewControllers(viewController.map { [$0] }, direction: direction, animated: true) { _ in
			self.view.isUserInteractionEnabled = true
			self.currentIndex = index
			self.currentSpeed = 1
		}
	}
	
	private func installEmptyView() {
		view.addSubview(emptyView)
		emptyView.translatesAutoresizingMaskIntoConstraints = false
		emptyView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
		emptyView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
		emptyView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
		emptyView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
	}

	@objc private func showEmptyViewIfNeeded() {
		emptyView.isHidden = router?.dataStore?.source != nil
		if emptyView.isHidden {
			return
		}
		selectSection(at: 0)
	}

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
		switch segue.identifier.flatMap(WalletSegue.init) {
		case .some(.create):
			router?.routeToCreateWallet(with: segue)
		case .some(.restore):
			router?.routeToRestoreWallet(with: segue)
		default:
			break
		}
    }
}

extension MainViewController: UIPageViewControllerDataSource {
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
		guard let viewControllers = router?.sections else { return nil }
		guard let index = viewControllers.firstIndex(of: viewController) else { return nil }
		return index == 0 ? nil : viewControllers[index - 1]
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
		guard let viewControllers = router?.sections else { return nil }
		guard let index = viewControllers.firstIndex(of: viewController) else { return nil }
		return index == viewControllers.count - 1 ? nil : viewControllers[index + 1]
	}
}

extension MainViewController: UIPageViewControllerDelegate {
	func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
		//view.isUserInteractionEnabled = false
	}

	func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
		guard let viewController = pageViewController.viewControllers?.first else { return }
		guard let viewControllers = router?.sections else { return }
		guard let index = viewControllers.firstIndex(of: viewController) else { return }
		currentIndex = index
		//view.isUserInteractionEnabled = true
	}
}

extension MainViewController: UIScrollViewDelegate {
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		guard let viewControllers = router?.sections else { return }
		let v = (scrollView.contentOffset.x - scrollView.bounds.width) / scrollView.bounds.width
		let width = sectionSelectorView.frame.inset(by: sectionSelectorView.layoutMargins).width
		let sectionWidth = width / CGFloat(viewControllers.count)
		let x = CGFloat(currentIndex) * sectionWidth + (sectionWidth - sectionCursor.frame.width) / 2
		let speed = CGFloat(currentSpeed) * sectionWidth
		sectionCursorOffset.constant = sectionSelectorView.layoutMargins.left + x + v * speed

		let delta = abs(v) * 0.5
		let direction = 2 - 2 * v.sign.rawValue - 1
		let targetIndex = currentIndex + direction * currentSpeed
		sectionSelectorView.arrangedSubviews[currentIndex].alpha = 1 - delta
		if targetIndex >= 0 && targetIndex < viewControllers.count {
			sectionSelectorView.arrangedSubviews[targetIndex].alpha = 0.5 + delta
		}
	}
	
	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		view.isUserInteractionEnabled = false
	}

	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		if !decelerate {
			view.isUserInteractionEnabled = true
		}
	}
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		view.isUserInteractionEnabled = true
	}
}
