// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

final class OnboardController: UIViewController {
	
	var router: (OnboardRoutingLogic & OnboardDataPassingLogic & UIPageViewControllerDelegate)?

	@IBOutlet private weak var navigationBar: UINavigationBar!
	@IBOutlet weak var pageControl: UIPageControl!
	
	@IBOutlet private weak var backButton: UIButton!
	@IBOutlet private weak var nextButton: UIButton!
	
	@IBOutlet private weak var bottomConstraint: NSLayoutConstraint!
	
	var pageViewController: UIPageViewController! {
		return (children.first as! UIPageViewController)
	}
	
	override var title: String? {
		didSet {
			navigationBar.topItem?.title = title
		}
	}
	var rightBarButtonItem: UIBarButtonItem? {
		set { navigationBar.topItem?.rightBarButtonItem = newValue }
		get { navigationBar.topItem?.rightBarButtonItem }
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	var isBackButtonEnabled: Bool {
		get { backButton.isEnabled }
		set { backButton.isEnabled = newValue }
	}
	var isNextButtonEnabled: Bool {
		get { nextButton.isEnabled }
		set { nextButton.isEnabled = newValue }
	}
	
	var isBackButtonHidden: Bool {
		get { backButton.isHidden }
		set { backButton.isHidden = newValue }
	}
	var isNextButtonHidden: Bool {
		get { nextButton.isHidden }
		set { nextButton.isHidden = newValue }
	}

	private var pendingIndex: Int = 0

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
		pageViewController.delegate = router
		router?.viewController = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		UIResponder.keyboardWillShowNotification.addObserver(self, selector: #selector(shrinkView))
		UIResponder.keyboardWillHideNotification.addObserver(self, selector: #selector(expandView))
	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		UIResponder.keyboardWillShowNotification.removeObserver(self)
		UIResponder.keyboardWillHideNotification.removeObserver(self)
	}

	@objc private func shrinkView(_ notification: Notification) {
		guard let userInfo = notification.userInfo else { return }
		guard let beginFrame = userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect else { return }
		guard let endFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else { return }
		guard beginFrame != endFrame else { return }
		let offset = endFrame.origin.y - beginFrame.origin.y + view.safeAreaInsets.bottom
		UIView.animate(withDuration: -1) {
			self.bottomConstraint.constant = offset
			self.view.layoutIfNeeded()
		}
	}
	@objc private func expandView(_ notification: Notification) {
		UIView.animate(withDuration: -1) {
			self.bottomConstraint.constant = 0
			self.view.layoutIfNeeded()
		}
	}

	@IBAction func back() {
		router?.routeToPreviousPage(with: nil)
	}
	@IBAction func next() {
		router?.routeToNextPage(with: nil)
	}

	@IBAction func dismiss() {
		dismiss(animated: true, completion: nil)
	}
}

extension UIViewController {
	var onboardController: OnboardController? {
		return parent?.parent as? OnboardController
	}
}
