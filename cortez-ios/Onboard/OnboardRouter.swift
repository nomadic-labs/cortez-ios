// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************


import UIKit

protocol OnboardRoutingLogic {
	
	var viewController: OnboardController? { get set }

	func routeToPreviousPage(with segue: UIStoryboardSegue?)
	func routeToNextPage(with segue: UIStoryboardSegue?)
}

protocol OnboardDataPassingLogic {
	var dataStore: OnboardDataStore? { get }
}

protocol OnboardPageRoutingLogic {
	func passData(from source: OnboardPageDataStore, to destination: inout OnboardDataStore)
	func routeToNextPage(with segue: UIStoryboardSegue?)
}

protocol OnboardPageDataStore {
}

protocol OnboardPageDataPassingLogic {
	var dataStore: OnboardPageDataStore? { get }
}

protocol OnboardPageViewController: UIViewController {

	var router: (OnboardPageRoutingLogic & OnboardPageDataPassingLogic)? { get set }

	func complete()
}

extension OnboardPageViewController {
	func complete() {
		// ignore
	}
}

class OnboardRouter: NSObject, OnboardRoutingLogic, OnboardDataPassingLogic {
	
	var dataStore: OnboardDataStore?

	weak var viewController: OnboardController? {
		didSet {
			viewController?.pageControl.numberOfPages = pageViewControllers.count
			route(to: .firstPage)
		}
	}

	var pageViewControllers: [OnboardPageViewController] {
		didSet {
			viewController?.pageControl.numberOfPages = pageViewControllers.count
			route(to: .firstPage)
		}
	}

	private var pendingIndex: Int = 0

	
	enum Destination {
		case firstPage
		case previousPage
		case nextPage
	}
	
	init(_ pageViewControllers: [OnboardPageViewController]) {
		self.pageViewControllers = pageViewControllers
	}

	func routeToPreviousPage(with segue: UIStoryboardSegue?) {
		route(to: .previousPage)
	}
	func routeToNextPage(with segue: UIStoryboardSegue?) {
		route(to: .nextPage)
	}
	
	func route(to destination: Destination) {
		guard let viewController = viewController else { return }
		if pageViewControllers.isEmpty { return }
		let currentPageIndex = viewController.pageControl.currentPage
		guard let nextPageIndex = index(for: destination) else { return }
		let currentPage = pageViewControllers[currentPageIndex]
		
		if nextPageIndex == pageViewControllers.count {
			currentPage.complete()
			return
		}

		let nextPage = pageViewControllers[nextPageIndex]
		guard let currentPageRouter = currentPage.router else { return }
		guard let nextPageRouter = nextPage.router else { return }
		guard let currentPageDataStore = currentPageRouter.dataStore else { return }
		guard var onboardDataStore = dataStore else { return }
		guard var nextPageDataStore = nextPageRouter.dataStore else { return }

		if nextPageIndex > currentPageIndex {
			currentPageRouter.passData(from: currentPageDataStore, to: &onboardDataStore)
			passData(from: onboardDataStore, to: &nextPageDataStore)
		} else if case .firstPage = destination {
			passData(from: onboardDataStore, to: &nextPageDataStore)
		}

		navigate(from: currentPage, at: currentPageIndex, to: nextPage, at: nextPageIndex)
	}

	func passData(from sourceDataStore: OnboardDataStore, to destinationDataStore: inout OnboardPageDataStore) {
		// ignore
	}
	
	func navigate(from source: UIViewController, at sourceIndex: Int, to destination: UIViewController, at destinationIndex: Int) {
		guard let viewController = viewController else { return }
		let direction: UIPageViewController.NavigationDirection = sourceIndex > destinationIndex ? .reverse : .forward
		if sourceIndex != destinationIndex {
			source.view.endEditing(true)
		}
		viewController.isBackButtonEnabled = true
		viewController.isNextButtonEnabled = true
		viewController.isBackButtonHidden = destinationIndex == 0
		viewController.isNextButtonHidden = false
		//viewController.isNextButtonHidden = destinationIndex == pageViewControllers.count - 1
		viewController.pageViewController.setViewControllers([destination], direction: direction, animated: true) { _ in
			self.updateViewController(with: destination, at: destinationIndex)
		}
	}
	
	private func index(for destination: Destination) -> Int? {
		guard let viewController = viewController else { return nil }
		let currentIndex = viewController.pageControl.currentPage
		switch destination {
		case .firstPage: return 0
		case .previousPage: return currentIndex == 0 ? nil : currentIndex - 1
		case .nextPage: return currentIndex == pageViewControllers.count ? nil : currentIndex + 1
		}
	}
	
	private func updateViewController(with destination: UIViewController, at index: Int) {
		guard let viewController = viewController else { return }
		viewController.title = destination.title
		viewController.rightBarButtonItem = destination.navigationItem.rightBarButtonItem
		viewController.pageControl.currentPage = index
	}
}

extension OnboardRouter: UIPageViewControllerDelegate {
	func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
		pendingIndex = pendingViewControllers.first.flatMap { pendingViewController in
			pageViewControllers.firstIndex { pendingViewController == $0 }
		} ?? 0
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool,
							previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
		guard completed else { return }
		updateViewController(with: pageViewControllers[pendingIndex], at: pendingIndex)
	}
}
