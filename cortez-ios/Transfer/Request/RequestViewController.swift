// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol RequestDisplayLogic: class {
	func show(_ viewModel: Transfer.QRCode.ViewModel)
}

class RequestViewController: UIViewController {

	var router: (RequestRoutingLogic & RequestDataPassingLogic)?
	var interactor: RequestBusinessLogic?

	@IBOutlet weak var backgroundView: UIView!
	@IBOutlet weak var qrCodeView: UIImageView!
	@IBOutlet weak var addressView: UILabel!

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = RequestRouter()
		let interactor = RequestInteractor(presenter: RequestPresenter(display: self))
		router.dataStore = interactor
		self.router = router
		self.interactor = interactor
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		backgroundView.layer.borderColor = UIColor.black.cgColor
		addressView.text = router?.dataStore?.address
		interactor?.execute(.init())
    }
	
	@IBAction func shareAddress() {
		guard let address = router?.dataStore?.address else { return }
		let activityViewController = UIActivityViewController(activityItems: [address], applicationActivities: nil)
		present(activityViewController, animated: true, completion: nil)
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
	
}

extension RequestViewController: RequestDisplayLogic {
	func show(_ viewModel: Transfer.QRCode.ViewModel) {
		qrCodeView.image = viewModel.image
	}
}
