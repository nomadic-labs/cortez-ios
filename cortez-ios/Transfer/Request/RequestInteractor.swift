// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import CoreImage

protocol RequestBusinessLogic {
	func execute(_ request: Transfer.QRCode.Request)
}

protocol RequestDataStore {
	var address: Address? { get set }
}

final class RequestInteractor: RequestBusinessLogic, RequestDataStore {

	var address: Address?
	
	let presenter: RequestPresentationLogic

	init(presenter: RequestPresentationLogic) {
		self.presenter = presenter
	}

	func execute(_ request: Transfer.QRCode.Request) {
		guard let data = address?.data(using: .ascii) else { return }
		let parameters: [String: Any] = ["inputMessage": data]
		guard let filter = CIFilter(name: "CIQRCodeGenerator", parameters: parameters) else { return }
		guard let image = filter.outputImage else { return }
		presenter.format(.init(image: image))
	}
}
