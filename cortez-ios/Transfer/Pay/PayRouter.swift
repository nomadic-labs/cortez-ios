// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit
import KeychainAccess

final class PayRouter: OnboardRouter {

	convenience init() {
		let pageViewControllers: [OnboardPageViewController] = [
			("ListAccounts", "select-account", "From"),
			("ListContacts", "select-contact", "To"),
			("Transfer", "confirm-payment", nil)
		].compactMap {
			let storyboard = UIStoryboard(name: $0, bundle: nil)
			let pageViewController = storyboard.instantiateViewController(withIdentifier: $1) as? OnboardPageViewController
			pageViewController?.title = $2
			return pageViewController
		}
		self.init(pageViewControllers)
		dataStore = PayInteractor()
	}

	override func passData(from source: OnboardDataStore, to destination: inout OnboardPageDataStore) {
		guard let source = source as? PayDataStore else { return }
		switch destination {
		case var destination as ListAccountsDataStore:
			passData(from: source, to: &destination)
		case var destination as ListContactsDataStore:
			passData(from: source, to: &destination)
		case var destination as ConfirmPaymentDataStore:
			passData(from: source, to: &destination)
		default:
			break
		}
	}
	func passData(from source: PayDataStore, to destination: inout ListAccountsDataStore) {
		guard let manager = try? Keychain().getData("public-key").flatMap(Source.init) else { return }
		destination.address = manager.address
	}
	func passData(from source: PayDataStore, to destination: inout ListContactsDataStore) {
		// ignore
	}
	func passData(from sourceDataStore: PayDataStore, to destinationDataStore: inout ConfirmPaymentDataStore) {
		guard let amount = sourceDataStore.amount else { return }
		guard let source = sourceDataStore.source else { return }
		guard let destination = sourceDataStore.destination else { return }
		destinationDataStore.transfer = .payment(
			Transfer.Payment(amount: amount, source: source, destination: destination)
		)
	}
}
