// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

enum Script {
	enum Manager {
		static let string = """
		{"code":[{"prim":"parameter","args":[{"prim":"or","args":[{"prim":"lambda","args":[{"prim":"unit"},
		{"prim":"list","args":[{"prim":"operation"}]}],"annots":["%%do"]},{"prim":"unit","annots":["%%default"]}]}]},
		{"prim":"storage","args":[{"prim":"key_hash"}]},{"prim":"code","args":[[[[{"prim":"DUP"},{"prim":"CAR"},
		{"prim":"DIP","args":[[{"prim":"CDR"}]]}]],{"prim":"IF_LEFT","args":[[{"prim":"PUSH","args":[{"prim":"mutez"},
		{"int":"0"}]},{"prim":"AMOUNT"},[[{"prim":"COMPARE"},{"prim":"EQ"}],{"prim":"IF","args":[[],[[{"prim":"UNIT"},
		{"prim":"FAILWITH"}]]]}],[{"prim":"DIP","args":[[{"prim":"DUP"}]]},{"prim":"SWAP"}],{"prim":"IMPLICIT_ACCOUNT"},
		{"prim":"ADDRESS"},{"prim":"SENDER"},[[{"prim":"COMPARE"},{"prim":"EQ"}],{"prim":"IF","args":[[],[[{"prim":"UNIT"},
		{"prim":"FAILWITH"}]]]}],{"prim":"UNIT"},{"prim":"EXEC"},{"prim":"PAIR"}],[{"prim":"DROP"},
		{"prim":"NIL","args":[{"prim":"operation"}]},{"prim":"PAIR"}]]}]]}],"storage":{"string":"%@"}}
		"""

		static let data = Data(
			2, 0, 0, 0, 193, 5, 0, 7, 100, 8, 94, 3, 108, 5, 95, 3, 109, 0, 0, 0, 3, 37, 100, 111, 4, 108, 0, 0, 0, 8, 37, 100, 101,
			102, 97, 117, 108, 116, 5, 1, 3, 93, 5, 2, 2, 0, 0, 0, 149, 2, 0, 0, 0, 18, 2, 0, 0, 0, 13, 3, 33, 3, 22, 5, 31, 2,
			0, 0, 0, 2, 3, 23, 7, 46, 2, 0, 0, 0, 106, 7, 67, 3, 106, 0, 0, 3, 19, 2, 0, 0, 0, 30, 2, 0, 0, 0, 4, 3, 25,
			3, 37, 7, 44, 2, 0, 0, 0, 0, 2, 0, 0, 0, 9, 2, 0, 0, 0, 4, 3, 79, 3, 39, 2, 0, 0, 0, 11, 5, 31, 2, 0, 0,
			0, 2, 3, 33, 3, 76, 3, 30, 3, 84, 3, 72, 2, 0, 0, 0, 30, 2, 0, 0, 0, 4, 3, 25, 3, 37, 7, 44, 2, 0, 0, 0, 0,
			2, 0, 0, 0, 9, 2, 0, 0, 0, 4, 3, 79, 3, 39, 3, 79, 3, 38, 3, 66, 2, 0, 0, 0, 8, 3, 32, 5, 61, 3, 109, 3, 66
		)
	}
}
