// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************


import Foundation
import Sodium

struct BinaryEncodedTransfer {
	var data: Data
	var fees: UInt64
	var gas: UInt64
}

extension BinaryEncodedTransfer: Decodable {

	enum Error: Swift.Error {
		case invalidPayload
		case invalidFee
		case invalidGas
	}

	enum CodingKeys: String, CodingKey {
		case data = "result"
		case fees = "total_fee"
		case gas = "total_gas"
	}
	
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		let result = try container.decode(String.self, forKey: .data)
		data = try Sodium().utils.hex2bin(result).map(Data.init(_:)) ?? { throw Error.invalidPayload }()
		fees = try UInt64(try container.decode(String.self, forKey: .fees)) ?? { throw Error.invalidFee }()
		gas = try UInt64(try container.decode(String.self, forKey: .gas)) ?? { throw Error.invalidGas }()
	}
}

protocol TransferEncodingLogic {
	
	typealias Completion = (Result<BinaryEncodedTransfer, Swift.Error>) -> Void

	func encode(_ transfer: CompiledTransfer, completion: @escaping Completion)
}

extension RemoteProcedure {
	init(_ transfer: CompiledTransfer) throws {
		switch transfer {
		case let .payment(transfers), let .delegation(transfers):
			self = try .forge(transfers)
		case .origination(let accounts):
			self = try .originate(accounts)
		}
	}
}

final class TransferEncoder: TransferEncodingLogic {
	
	private var call: Call?

	func encode(_ transfer: CompiledTransfer, completion: @escaping Completion) {
		do {
			call = Call(remoteProcedure: try RemoteProcedure(transfer), session: Provider.shared.get() ?? .shared)
			call?.enqueue(
				onValue: { (encodedTransfer: BinaryEncodedTransfer) in completion(.success(encodedTransfer)) },
				onError: { error in completion(.failure(error)) }
			)
		} catch {
			completion(.failure(error))
		}
	}
}
