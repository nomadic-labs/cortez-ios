// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ConfirmTransferPresentationLogic {
	func format(_ response: Transfer.Encoding.Response)
	func format(_ response: Transfer.Injection.Response)
}

struct ConfirmTransferPresenter: ConfirmTransferPresentationLogic {

	weak var display: ConfirmTransferDisplayLogic?

	let formatter: NumberFormatter = .tez()
	
	let systemFont = UIFont.systemFont(ofSize: 23, weight: .regular)
	let nameFont = UIFont.systemFont(ofSize: 31, weight: .semibold)
	let cortezFont = UIFont(name: "cortez", size: 31) ?? UIFont.systemFont(ofSize: 31, weight: .semibold)

	// TODO: refactor for localization
	func format(_ response: Transfer.Encoding.Response) {
		switch response {
		case let .encoding(.delegation(delegation)):
			format(delegation)
		case let .encoding(.origination(origination)):
			format(origination)
		case let .encoding(.payment(payment)):
			format(payment)
		
		case .encoded(let fees):
			guard let formattedFees = formatter.string(for: Double(fees).tez) else { return } // TODO: error
			let systemFont = UIFont.systemFont(ofSize: 16, weight: .semibold)
			guard let cortezFont = UIFont(name: "cortez", size: 16) else { return }
			let attributedString = NSMutableAttributedString(string: "fees: ", attributes: [.font: systemFont])
			let attributedFees = NSAttributedString(string: formattedFees, attributes: [.font: cortezFont])
			attributedString.append(attributedFees)
			display?.show(.encoded(fees: attributedString))
		
		case .failed:
			display?.show(.failed(message: "Something went wrong with your payment."))
		}
	}
	
	func format(_ response: Transfer.Injection.Response) {
		guard response.error == nil else {
			display?.show(.init(message: "Something went wrong with your payment."))
			return
		}
		display?.show(.init(message: nil))
	}

	private func format(_ delegation: Transfer.Delegation) {
		let source = (delegation.source.name ?? delegation.source.address).withoutLineBreaks
		let delegate = delegation.delegate?.name.withoutLineBreaks

		let attributedString = NSMutableAttributedString()
	
		if let delegate = delegate {
			attributedString.append(NSAttributedString(string: "Delegate ", attributes: [.font: systemFont]))
			attributedString.append(NSAttributedString(string: source, attributes: [.font: nameFont]))
			attributedString.append(NSAttributedString(string: " to ", attributes: [.font: systemFont]))
			attributedString.append(NSAttributedString(string: delegate, attributes: [.font: nameFont]))
			attributedString.append(NSAttributedString(string: "?", attributes: [.font: systemFont]))
		} else {
			attributedString.append(NSAttributedString(string: "Stop ", attributes: [.font: systemFont]))
			attributedString.append(NSAttributedString(string: source, attributes: [.font: nameFont]))
			attributedString.append(NSAttributedString(string: " from delegating?", attributes: [.font: systemFont]))
		}
		display?.show(.encoding(question: attributedString))
	}

	private func format(_ origination: Transfer.Origination) {
		let name = origination.name.withoutLineBreaks
		let amount = origination.amount
		let delegate = origination.delegate?.name.withoutLineBreaks
		guard let formattedBalance = formatter.string(for: Double(amount).tez) else { return }
		
		let attributedString = NSMutableAttributedString(string: "Create account ", attributes: [.font: systemFont])
		attributedString.append(NSAttributedString(string: name, attributes: [.font: nameFont]))
		attributedString.append(NSAttributedString(string: " with ", attributes: [.font: systemFont]))
		attributedString.append(NSAttributedString(string: formattedBalance, attributes: [.font: cortezFont]))
		if let delegate = delegate {
			attributedString.append(NSAttributedString(string: " and delegate to ", attributes: [.font: systemFont]))
			attributedString.append(NSAttributedString(string: delegate, attributes: [.font: nameFont]))
		}
		attributedString.append(NSAttributedString(string: "?", attributes: [.font: systemFont]))

		display?.show(.encoding(question: attributedString))
	}

	private func format(_ payment: Transfer.Payment) {
		let amount = payment.amount
		let source = (payment.source.name ?? payment.source.address).withoutLineBreaks
		let destination = payment.destination.name
		guard let formattedAmount = formatter.string(for: Double(amount).tez) else { return } // TODO: error
		
		let attributedString = NSMutableAttributedString(string: "Send ", attributes: [.font: systemFont])
		attributedString.append(NSAttributedString(string: formattedAmount, attributes: [.font: cortezFont]))
		attributedString.append(NSMutableAttributedString(string: " from ", attributes: [.font: systemFont]))
		attributedString.append(NSMutableAttributedString(string: source, attributes: [.font: nameFont]))
		attributedString.append(NSMutableAttributedString(string: " to ", attributes: [.font: systemFont]))
		attributedString.append(NSMutableAttributedString(string: destination, attributes: [.font: nameFont]))
		attributedString.append(NSMutableAttributedString(string: "?", attributes: [.font: systemFont]))

		display?.show(.encoding(question: attributedString))
	}
}
