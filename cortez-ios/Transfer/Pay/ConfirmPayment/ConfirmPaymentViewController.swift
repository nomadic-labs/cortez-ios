// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ConfirmTransferDisplayLogic: class {
	func show(_ viewModel: Transfer.Encoding.ViewModel)
	func show(_ viewModel: Transfer.Injection.ViewModel)
}

class ConfirmPaymentViewController: UIViewController, OnboardPageViewController {

	var router: (OnboardPageRoutingLogic & OnboardPageDataPassingLogic)?
	var interactor: ConfirmPaymentBusinessLogic?

	var attributedTexts: [NSAttributedString?] = [nil, nil]

	@IBOutlet weak var contentView: UITableView!
	@IBOutlet weak var yesButton: UIButton!
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}

	private func setup() {
		let router = ConfirmPaymentRouter()
		let interactor = ConfirmPaymentInteractor(
			encoder: TransferEncoder(),
			validator: TransferValidator(),
			injector: TransferInjector(),
			presenter: ConfirmTransferPresenter(display: self)
		)
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		contentView.rowHeight = UITableView.automaticDimension
		contentView.estimatedRowHeight = 600

		let refreshControl = UIRefreshControl()
		refreshControl.tintColor = .white
		refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
		contentView.refreshControl = refreshControl
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		onboardController?.isNextButtonHidden = true
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.forge()
	}

	@objc func refresh() {
		if contentView.isDragging { return }
		forge()
	}

	func forge() {
		interactor?.execute(Transfer.Encoding.Request())
		yesButton.isEnabled = false
	}
	
	@IBAction func inject() {
		interactor?.execute(Transfer.Injection.Request())
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension ConfirmPaymentViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return attributedTexts[0] == nil ? 0 : attributedTexts[1] == nil ? 1 : 2
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cellIdentifiers: [String] = ["question", "NB"]
		let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[indexPath.row], for: indexPath)
		if var cell = cell as? ConfirmPaymentTableViewCell {
			cell.attributedText = attributedTexts[indexPath.row]
		}
		return cell
	}
}

extension ConfirmPaymentViewController: UITableViewDelegate {
	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		guard let refreshControl = contentView.refreshControl, refreshControl.isRefreshing else { return }
		forge()
	}
}

extension ConfirmPaymentViewController: ConfirmTransferDisplayLogic {
	func show(_ viewModel: Transfer.Encoding.ViewModel) {
		switch viewModel {
		case .encoding(let operation):
			show(operation, at: 0)
		case .encoded(let price):
			show(price, at: 1)
			yesButton.isEnabled = true
			contentView.refreshControl?.endRefreshing()
		case let .failed(message):
			showErrorMessage(message, handler: forge)
			contentView.refreshControl?.endRefreshing()
		}
	}
	func show(_ viewModel: Transfer.Injection.ViewModel) {
		if let message = viewModel.message {
			showErrorMessage(message, handler: inject)
			return
		}
		dismiss(animated: true, completion: nil)
	}
	
	private func show(_ attributedText: NSAttributedString?, at index: Int) {
		let previousAttributedText = attributedTexts[index]
		attributedTexts[index] = attributedText
		let animations: [UITableView.RowAnimation] = [.fade, .left]
		switch (previousAttributedText, attributedText) {
		case (.none, .some): contentView.insertRows(at: [IndexPath(row: index, section: 0)], with: animations[index])
		case (.some, .some): contentView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
		default: break // should not happen
		}
	}
	
	private func showErrorMessage(_ message: String, handler: @escaping () -> Void) {
		let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		let cancelButton = UIAlertAction(title: "Cancel", style: .destructive) { _ in
			//self.performSegue(withIdentifier: "unwind", sender: nil)
			self.dismiss(animated: true, completion: nil)
		}
		let retryButton = UIAlertAction(title: "Retry", style: .default) { _ in
			handler()
		}
		alert.addAction(cancelButton)
		alert.addAction(retryButton)
		present(alert, animated: true, completion: nil)
	}
}

extension ConfirmPaymentViewController {
	class func instantiateFromStoryboard() -> ConfirmPaymentViewController? {
		let storyboard = UIStoryboard(name: "Transfer", bundle: nil)
		return storyboard.instantiateViewController(withIdentifier: "confirm-payment") as? ConfirmPaymentViewController
	}
}
