// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import KeychainAccess

extension Source {
	static var manager: Source? {
		try? Keychain().getData("public-key").flatMap(Source.init)
	}
}

enum CompiledTransfer {
	case delegation(Transfers)
	case origination(Accounts)
	case payment(Transfers)
}

extension Transfer {
	func compile() -> CompiledTransfer? {
		switch self {
		case let .delegation(delegation):
			return compile(delegation).map { .delegation($0) }
		case let .origination(origination):
			return compile(origination).map { .origination($0) }
		case let .payment(payment):
			return compile(payment).map { .payment($0) }
		}
	}
	
	func compile(_ delegation: Delegation) -> Transfers? {
		guard let manager = Source.manager else { return nil }
		let source = delegation.source
		let delegate = delegation.delegate
		if source.address.isOriginated { // source is an originated manager contract
			let parameters = Transfers.Destination.Parameters(
				entrypoint: .do,
				expression: .flatten(
					.drop,
					.nil(.operation),
					.flatten {
						guard let delegate = delegate else {
							return [.none(.keyHash)]
						}
						return [.some(.keyHash(delegate.address))]
					},
					.setDelegate,
					.cons
				)
			)
			return Transfers(
				source: manager,
				destinations: [
					.init(address: source.address, amount: 0, parameters: parameters)
				]
			)
		}
		// TODO: implement delegation for an implicit account
		return nil
	}
	
	func compile(_ origination: Origination) -> Accounts? {
		guard let manager = Source.manager else { return nil }
		guard let data = String(format: Script.Manager.string, manager.address).data(using: .utf8) else { return nil }
		guard let object = try? JSONSerialization.jsonObject(with: data, options: []) else { return nil }
		let amount = origination.amount
		let delegate = origination.delegate
		return Accounts(
			source: manager,
			destinations: [
				.init(balance: amount, delegate: delegate?.address, script: object)
			]
		)
	}
	
	func compile(_ payment: Payment) -> Transfers? {
		guard let manager = Source.manager else { return nil }
		let source = payment.source
		let destination = payment.destination
		let amount = payment.amount
		if source.address.isOriginated { // source is an originated manager contract
			let parameters = Transfers.Destination.Parameters(
				entrypoint: .do,
				expression: .flatten(
					.drop,
					.nil(.operation),
					.flatten {
						if destination.address.isOriginated {
							return [.push(.address(destination.address)), .contract(.unit), .assertSome]
						}
						return [.push(.keyHash(destination.address)), .implicitAccount]
					},
					.push(.mutez(amount)),
					.UNIT,
					.transferTokens,
					.cons
				)
			)
			return Transfers(
				source: Source(address: manager.address, publicKey: manager.publicKey),
				destinations: [
					.init(address: source.address, amount: 0, parameters: parameters)
				]
			)
		}
		return Transfers(
			source: Source(address: source.address, publicKey: manager.publicKey),
			destinations: [
				.init(address: destination.address, amount: amount, parameters: nil)
			]
		)
	}
}
