// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

import Sodium
import MnemonicKit
import KeychainAccess
import Business

typealias PrivateKey = Data

extension Data {
	func sign(with key: PrivateKey) -> Data? { // return signature
		let sodium = Sodium()
		guard let hash = sodium.genericHash.hash(message: [0x3] + bytes) else { return nil }
		return Sodium().sign.signature(message: hash, secretKey: key.bytes).map(Data.init(_:))
	}
	
	func signed(with key: PrivateKey) -> Data? {
		return sign(with: key).map { self + $0 }
	}
}

protocol TransferInjectionLogic {
	
	typealias Completion = (Result<String, Swift.Error>) -> Void

	func inject(_ data: Data, completion: @escaping Completion) -> Disposable
}

extension Call: Disposable {
	func dispose() {
		cancel()
	}
}

final class TransferInjector: TransferInjectionLogic {
	
	typealias Completion = TransferInjectionLogic.Completion

	private var call: Call?
	
	enum Error: Swift.Error {
		case invalidSignature
	}

	func inject(_ data: Data, completion: @escaping Completion) -> Disposable {
		do {
			guard let signedData = try fetchSecretKey().flatMap(data.signed) else {
				completion(.failure(Error.invalidSignature))
				return AnyDisposable.empty
			}
			let call = Call(remoteProcedure: .inject(signedData), session: Provider.shared.get() ?? .shared)
			call.enqeue(jsonDecoder: .init()) { result in
				completion(result.mapError { $0 as Swift.Error })
			}
			return call
		} catch {
			completion(.failure(error))
		}
		return AnyDisposable.empty
	}

	private func fetchSecretKey() throws -> PrivateKey? {
		if let value = try Keychain().getData("biometry-secret-key") {
			return value
		}
		return try Keychain().getData("secret-key")
	}
}

extension TransferInjectionLogic {
	func inject(_ data: Data) -> Work<Address> {
		return { promise in self.inject(data) { result in promise.fulfill(with: result) } }
	}
}
