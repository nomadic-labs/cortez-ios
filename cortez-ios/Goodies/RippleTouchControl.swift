// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

class RippleTouchControl: NSObject {

	var shouldProcessRippleWithScrollViewGestures = true

	weak var view: UIView?
	weak var rippleView: RippleView?
	
	weak var gestureRecognizer: UILongPressGestureRecognizer?
	
	private(set) var isTapOutOfBounds = false
	
	init(with view: UIView) {
		super.init()
		
		let rippleView = RippleView(frame: view.bounds)
		view.addSubview(rippleView)
		self.view = view
		self.rippleView = rippleView
		
		let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(action))
		gestureRecognizer.minimumPressDuration = 0
		gestureRecognizer.delegate = self
		gestureRecognizer.cancelsTouchesInView = false
		gestureRecognizer.delaysTouchesEnded = false
		view.addGestureRecognizer(gestureRecognizer)
		self.gestureRecognizer = gestureRecognizer
	}
	
	deinit {
		guard let gestureRecognizer = gestureRecognizer else { return }
		view?.removeGestureRecognizer(gestureRecognizer)
		gestureRecognizer.delegate = nil
	}
	
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return !shouldProcessRippleWithScrollViewGestures
		&& otherGestureRecognizer.view is UIScrollView
		&& !(otherGestureRecognizer is UITapGestureRecognizer)
		&& !(otherGestureRecognizer is UILongPressGestureRecognizer)
	}
	
	@objc func action(_ gestureRecognizer: UILongPressGestureRecognizer) {
		guard let view = view, let rippleView = rippleView else { return }
		switch gestureRecognizer.state {
		case .began:
			rippleView.beginRippleTouchDown(
				at: gestureRecognizer.location(in: view),
				animated: true,
				completion: nil
			)
		case .possible: break // ignored
		case .changed:
			let touchLocation = gestureRecognizer.location(in: view)
			let isPointContainedInBounds = view.bounds.contains(touchLocation)
			if isPointContainedInBounds && isTapOutOfBounds {
				isTapOutOfBounds = false
				rippleView.fadeInRipple(animated: true, completion: nil)
			} else if !isPointContainedInBounds && !isTapOutOfBounds {
				isTapOutOfBounds = true
				rippleView.fadeOutRipple(animated: true, completion: nil)
			}
		case .ended: rippleView.beginRippleTouchUp(animated: true, completion: nil)
		case .cancelled, .failed: rippleView.cancelAllRipples(animated: true, completion: nil)
		@unknown default:
			break
		}
	}
}

extension RippleTouchControl: UIGestureRecognizerDelegate {
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
}
