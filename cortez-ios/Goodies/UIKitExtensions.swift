// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

extension UITextInput {

	var text: String? {
		guard let allDocument = textRange(from: beginningOfDocument, to: endOfDocument) else { return nil }
		return text(in: allDocument)
	}
	
	var prefix: String? {
		return prefixRange.flatMap(text)
	}

	var prefixRange: Range<String.Index>? {
		// get full text
		guard let text = text else { return nil }

		// get textView cursor position
		guard let position = selectedTextRange?.start else { return nil }
		let offset = self.offset(from: beginningOfDocument, to: position)
		
		// find prefix bounds
		let end = text.index(text.startIndex, offsetBy: offset)
		let start = text[..<end].lastIndex(of: " ").map { text.index($0, offsetBy: 1) } ?? text.startIndex

		return start..<end
	}
	
	var wordRange: Range<String.Index>? {
		guard let prefixRange = prefixRange else { return nil }
		return wordRange(withPrefixRange: prefixRange)
	}
	
	func replace(_ range: Range<String.Index>, withText text: String) {
		guard let textRange = textRange(range) else { return }
		replace(textRange, withText: text)
	}

	func text(in range: Range<String.Index>) -> String? {
		guard let text = text else { return nil }
		return String(text[range])
	}
	
	func textRange(_ range: Range<String.Index>) -> UITextRange? { // holy moly
		guard let text = text else { return nil }
		let stringStart = text.distance(from: text.startIndex, to: range.lowerBound)
		let stringEnd = text.distance(from: text.startIndex, to: range.upperBound)
		guard let textInputStart = position(from: beginningOfDocument, offset: stringStart) else { return nil }
		guard let textInputEnd = position(from: beginningOfDocument, offset: stringEnd) else { return nil }
		return textRange(from: textInputStart, to: textInputEnd)
	}
	
	func wordRange(withPrefixRange prefixRange: Range<String.Index>) -> Range<String.Index>? {
		guard let text = text else { return nil }
		let start = prefixRange.lowerBound
		let end = text[start...].firstIndex(of: " ") ?? text.endIndex
		return start..<end
	}
}

extension UIStackView {
	func removeAllArrangedSubviews() {
		while let arrangedSubview = arrangedSubviews.last {
			removeArrangedSubview(arrangedSubview)
		}
	}
	
	func addArrangedSubviews(_ views: [UIView]) {
		views.forEach(addArrangedSubview)
	}
}

extension NSRegularExpression {
	func hasMatch(in textInput: UITextInput?) -> Bool {
		guard let text = textInput?.text else { return false }
		return firstMatch(in: text, options: [], range: NSRange(location: 0, length: text.count)) != nil
	}
}

extension UIColor {
	convenience init(rgba: Int) {
		self.init(
			red: CGFloat((rgba >> 24) & 0xff) / CGFloat(255),
			green: CGFloat((rgba >> 16) & 0xff) / CGFloat(255),
			blue: CGFloat((rgba >> 8) & 0xff) / CGFloat(255),
			alpha: CGFloat(rgba & 0xff) / CGFloat(255)
		)
	}
	
	convenience init(rgb: Int) {
		self.init(rgba: (rgb << 8) | 0xff)
	}
}

extension UISwitch {
	
	private final class SwitchTargetAction {
		
		static var key: UInt8 = 0
		
		let body: ((Bool) -> Void)?
		
		init(_ body: ((Bool) -> Void)? = nil) {
			self.body = body
		}
		
		@objc func action(_ sender: UISwitch) {
			body?(sender.isOn)
		}
	}
	
	private var target: SwitchTargetAction? {
		get {
			return objc_getAssociatedObject(self, &SwitchTargetAction.key) as? SwitchTargetAction
		}
		set {
			if let target = self.target {
				removeTarget(target, action: #selector(target.action), for: .valueChanged)
			}
			objc_setAssociatedObject(self, &SwitchTargetAction.key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
		}
	}

	var action: ((Bool) -> Void)? {
		get {
			return target?.body
		}
		set {
			guard let body = newValue else {
				target = nil
				return
			}
			let target = SwitchTargetAction(body)
			addTarget(target, action: #selector(target.action), for: .valueChanged)
			self.target = target
		}
	}
}

extension UIPageViewController {
	var scrollView: UIScrollView? {
		return view.subviews.first { $0.isKind(of: UIScrollView.self) } as? UIScrollView
	}
}
