// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

extension CIImage {
	
	var transparent: CIImage? {
		return inverted?.blackTransparent
	}
	
	/// Inverts the colors.
	var inverted: CIImage? {
		guard let invertedColorFilter = CIFilter(name: "CIColorInvert") else { return nil }
		
		invertedColorFilter.setValue(self, forKey: "inputImage")
		return invertedColorFilter.outputImage
	}
	
	/// Converts all black to transparent.
	var blackTransparent: CIImage? {
		guard let blackTransparentFilter = CIFilter(name: "CIMaskToAlpha") else { return nil }
		blackTransparentFilter.setValue(self, forKey: "inputImage")
		return blackTransparentFilter.outputImage
	}
	
	func tinted(using color: UIColor) -> CIImage? {
		guard let transparentQRImage = transparent else { return nil }
		guard let filter = CIFilter(name: "CIMultiplyCompositing") else { return nil }
		guard let colorFilter = CIFilter(name: "CIConstantColorGenerator") else { return nil }
		
		let ciColor = CIColor(color: color)
		colorFilter.setValue(ciColor, forKey: kCIInputColorKey)
		let colorImage = colorFilter.outputImage
		
		filter.setValue(colorImage, forKey: kCIInputImageKey)
		filter.setValue(transparentQRImage, forKey: kCIInputBackgroundImageKey)
		
		return filter.outputImage
	}
}
