// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

extension Sequence {
	func takeWhile(_ shouldContinue: (Int, Element) throws -> Bool) rethrows -> [Element] {
		var taken = [Element]()
		for element in self where try shouldContinue(taken.count, element) {
			taken.append(element)
		}
		return taken
	}
	func sideEffect(_ body: (Element) -> Void) -> Self {
		forEach(body)
		return self
	}
}

extension Optional {
	func filter(_ isIncluded: (Wrapped) throws -> Bool) rethrows -> Optional {
		guard let wrapped = self else { return self }
		return try isIncluded(wrapped) ? wrapped : nil
	}
	mutating func mutate<T>(_ trigger: T, reducer: (Optional, T) -> Optional) {
		self = reducer(self, trigger)
	}
}

extension String {
	
	static var empty: String = ""

	var indexTitle: String {
		if isEmpty { return .empty }
		return String(self[startIndex..<index(after: startIndex)])
			.folding(options: .diacriticInsensitive, locale: .current)
			.uppercased()
	}
}

extension String {
	func prefixing<T: StringProtocol>(toLength newLength: Int, withPrefix prefixString: T, startingAt prefixIndex: Int) -> String {
		let prefixLength = newLength - count
		guard prefixLength > 0 else { return self }
		return "".padding(toLength: prefixLength, withPad: prefixString, startingAt: prefixIndex) + self
	}
}

extension String {
	var withoutLineBreaks: String {
		replacingOccurrences(of: " ", with: "\u{a0}").replacingOccurrences(of: "-", with: "\u{2011}")
	}
}

func union<T>(lhs: Set<T>, rhs: Set<T>) -> Set<T> {
	return lhs.union(rhs)
}


typealias Byte = UInt8

extension Byte {
	//maskOfFirstNBits
	static func firstNBitsMask(_ n: Int) -> Byte {
		return ~((Byte(1) << (8 - n)) - Byte(1))
	}
}

extension Result {
	var error: Failure? {
		guard case let .failure(error) = self else { return nil }
		return error
	}
	var value: Success? {
		guard case let .success(value) = self else { return nil }
		return value
	}
}
