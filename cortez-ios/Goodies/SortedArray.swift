// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

struct SortedArray<Element> where Element : Comparable {
	
	fileprivate var elements: [Element]
	
	init(_ elements: [Element]) {
		self.elements = elements.sorted()
	}
	
	func insertIndex(for element: Element) -> (Int, Bool) {
		var start = 0, end = elements.count
		while start < end {
			let middle = start + (end - start) / 2
			if elements[middle] == element {
				return (middle, true)
			}
			if elements[middle] < element {
				start = middle + 1
			} else {
				end = middle
			}
		}
		return (start, false)
	}

	mutating func insert(_ element: Element) {
		let (index, _) = insertIndex(for: element)
		elements.insert(element, at: index)
	}
}

extension SortedArray: Collection {
	
	var startIndex: Int {
		return elements.startIndex
	}
	
	var endIndex: Int {
		return elements.endIndex
	}
	
	subscript(index: Int) -> Element {
		get { return elements[index] }
	}
	
	func index(after i: Int) -> Int {
		return elements.index(after: i)
	}
}

extension Array where Element : Comparable {
	init(_ sorted: SortedArray<Element>) {
		self.init(sorted.elements)
	}
}
