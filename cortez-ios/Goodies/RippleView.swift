// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

let rippleDefaultAlpha: CGFloat = 0.16

class RippleView: UIView {

	var rippleColor = UIColor(white: 0, alpha: rippleDefaultAlpha) {
		didSet {
			activeRippleLayer?.fillColor = rippleColor.cgColor
		}
	}

	var isRippleBounded = true {
		didSet {
			updateRippleStyle()
		}
	}
	
	private let maskLayer = CAShapeLayer()

	private weak var activeRippleLayer: RippleLayer?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		isUserInteractionEnabled = false
		backgroundColor = .clear
		autoresizingMask = [.flexibleWidth, .flexibleHeight]
		layer.masksToBounds = true
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		updateRippleStyle()
		if let superviewFrame = superview?.bounds.standardized {
			frame = superviewFrame
		}
	}
	
	override func layoutSublayers(of layer: CALayer) {
		super.layoutSublayers(of: layer)
		layer.sublayers?.forEach {
			$0.frame = self.bounds.standardized
			$0.setNeedsLayout()
		}
	}
	
	private func updateRippleStyle() {
		layer.masksToBounds = isRippleBounded
		if isRippleBounded {
			if let shadowPath = superview?.layer.shadowPath {
				maskLayer.path = shadowPath
				layer.mask = maskLayer
			} else {
				superview?.clipsToBounds = true
			}
		} else {
			layer.mask = nil
			superview?.clipsToBounds = false
		}
	}
	
	func cancelAllRipples(animated: Bool, completion: (() -> Void)?) {
		guard let sublayers = layer.sublayers else {
			completion?()
			return
		}
		guard animated else {
			sublayers.lazy.filter { $0 is RippleLayer }.forEach { $0.removeFromSuperlayer() }
			completion?()
			return
		}
		let latestBeginTouchDownRippleTime = sublayers.lazy
			.compactMap { ($0 as? RippleLayer)?.rippleTouchDownStartTime }
			.max() ?? .leastNormalMagnitude
		
		let group = DispatchGroup()
		sublayers.forEach {
			guard let layer = $0 as? RippleLayer else { return }
			if layer.isStartAnimationActive {
				layer.rippleTouchDownStartTime = latestBeginTouchDownRippleTime + rippleFadeOutDelay
			}
			group.enter()
			layer.endRipple(animated: animated, completion: group.leave)
		}
		group.notify(queue: .main) { completion?() }
	}
	
	func beginRippleTouchDown(at point: CGPoint, animated: Bool, completion: (() -> Void)?) {
		let rippleLayer = RippleLayer()
		rippleLayer.fillColor = rippleColor.cgColor
		rippleLayer.frame = bounds
		layer.addSublayer(rippleLayer)
		rippleLayer.startRipple(at: point, animated: animated, completion: completion)
		activeRippleLayer = rippleLayer
	}
	
	func beginRippleTouchUp(animated: Bool, completion: (() -> Void)?) {
		activeRippleLayer?.endRipple(animated: animated, completion: completion)
	}
	
	func fadeInRipple(animated: Bool, completion: (() -> Void)?) {
		activeRippleLayer?.fadeInRipple(animated: animated, completion: completion)
	}
	
	func fadeOutRipple(animated: Bool, completion: (() -> Void)?) {
		activeRippleLayer?.fadeOutRipple(animated: animated, completion: completion)
	}
}
