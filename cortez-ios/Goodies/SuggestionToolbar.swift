// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

@objc protocol SuggestionToolbarDelegate: class {
	func suggestionToolbar(_ suggestionToolbar: SuggestionToolbar, didSelectSuggestion suggestion: String)
}

class SuggestionToolbar: UIToolbar {

	var suggestions: [String] {
		get {
			return items?.lazy.compactMap { $0.title }.filter { !$0.isEmpty } ?? []
		}
		set {
			guard let items = items else { return }
			var iterator = newValue.makeIterator()
			items.lazy.filter { $0.title != nil && $0 != self.actionButton }.forEach {
				let title = iterator.next()
				$0.title = title ?? " "
				$0.isEnabled = title != nil
			}
		}
	}
	
	var actionButton: UIBarButtonItem? {
		willSet {
			guard actionButton != nil else { return }
			items?.removeLast()
		}
		didSet {
			guard let actionButton = actionButton else {
				addOptionalSuggestionButton()
				return
			}
			removeOptionalSuggestionButton()
			items?.append(actionButton)
		}
	}

	@IBOutlet weak var suggestionDelegate: SuggestionToolbarDelegate?
	
	private var optionalSuggestionButton: UIBarButtonItem?

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}
	
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		suggestions = []
		items?.forEach {
			guard $0.title != nil else { return }
			$0.target = self
			$0.action = #selector(selectSuggestion)
			optionalSuggestionButton = $0
		}
	}
	
	@objc private func selectSuggestion(_ barButtonItem: UIBarButtonItem) {
		guard let suggestion = barButtonItem.title else { return }
		suggestionDelegate?.suggestionToolbar(self, didSelectSuggestion: suggestion)
	}
	
	private func addOptionalSuggestionButton() {
		guard var items = items else { return }
		guard let optionalSuggestionButton = optionalSuggestionButton else { return }
		guard items.firstIndex(of: optionalSuggestionButton) == nil else { return }
		items.insert(optionalSuggestionButton, at: items.count - 2)
		setItems(items, animated: true)
	}
	
	private func removeOptionalSuggestionButton() {
		guard var items = items else { return }
		guard let optionalSuggestionButton = optionalSuggestionButton else { return }
		guard let index = items.firstIndex(of: optionalSuggestionButton) else { return }
		items.remove(at: index)
		setItems(items, animated: true)
	}
}
