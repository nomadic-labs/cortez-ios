// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

protocol Executable {
	
	typealias Completion = (Error?) -> Void
	
	func execute(_ completion: @escaping Completion)
}

struct AnyExecutable: Executable {
	
	typealias Body = (@escaping Completion) -> Void
	
	private let body: Body
	
	init(_ body: @escaping Body) {
		self.body = body
	}
	func execute(_ completion: @escaping Completion) {
		body(completion)
	}
}

extension Array: Executable where Element: Executable {
	func execute(_ completion: @escaping Executable.Completion) {
		ExecutableWalker(iterator: makeIterator(), completion: completion).walk()
	}
}

private final class ExecutableWalker<Element: Executable> {
	
	private var iterator: AnyIterator<Element>
	private let completion: Executable.Completion
	
	init<T: IteratorProtocol>(iterator: T, completion: @escaping Executable.Completion) where Element == T.Element {
		self.iterator = AnyIterator(iterator)
		self.completion = completion
	}

	func walk() {
		guard let element = iterator.next() else {
			return completion(nil)
		}
		element.execute {
			if let error = $0 {
				self.completion(error)
				return
			}
			self.walk()
		}
	}
}
