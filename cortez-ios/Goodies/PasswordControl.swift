// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

// 1 upper case character, 1 lower case character, 1 special character ($@!%*#?&), 8 minimum character length
// ^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$

@objc protocol PasswordControlDelegate: class {
	@objc optional func passwordControlStateDidChange(_ passwordControl: PasswordControl)
	@objc optional func passwordControlPasswordFieldDidReturn(_ passwordControl: PasswordControl)
	@objc optional func passwordControlPasswordFieldDidFailToReturn(_ passwordControl: PasswordControl)
	@objc optional func passwordControlPasswordConfirmationFieldDidReturn(_ passwordControl: PasswordControl)
	@objc optional func passwordControlPasswordConfirmationFieldDidFailToReturn(_ passwordControl: PasswordControl)
}

class PasswordControl: NSObject {

	struct State: OptionSet {

		let rawValue: Int
		
		static let validPassword = State(rawValue: 1 << 0)
		static let confirmedPassword = State(rawValue: 1 << 1)
	}
	
	private(set) var state: State = [] {
		didSet {
			passwordConfirmationField?.isEnabled = isPasswordValid
			delegate?.passwordControlStateDidChange?(self)
		}
	}
	
	var isPasswordValid: Bool {
		return state.contains(.validPassword)
	}
	var isPasswordConfirmed: Bool {
		return state.contains(.confirmedPassword)
	}

	@IBInspectable var pattern: String? {
		get { return regularExpression?.pattern }
		set {
			do {
				regularExpression = try newValue.map { try NSRegularExpression(pattern: $0, options: []) }
			} catch {
				regularExpression = nil
			}
		}
	}
	@IBInspectable var successImage: UIImage?
	@IBInspectable var failureImage: UIImage?
	@IBInspectable var tintColor: UIColor? {
		didSet {
			passwordField?.rightView?.tintColor = tintColor
			passwordConfirmationField?.rightView?.tintColor = tintColor
		}
	}
	
	@IBOutlet weak var passwordField: UITextField? {
		willSet {
			unbind(passwordField)
		}
		didSet {
			bind(passwordField)
		}
	}
	@IBOutlet weak var passwordConfirmationField: UITextField? {
		willSet {
			unbind(passwordConfirmationField)
		}
		didSet {
			bind(passwordConfirmationField)
		}
	}
	
	@IBOutlet weak var delegate: PasswordControlDelegate?
	
	private var regularExpression: NSRegularExpression?
	
	private func bind(_ textField: UITextField?) {
		guard let textField = textField else { return }
		textField.delegate = self
		textField.addTarget(self, action: #selector(updateState), for: .editingChanged)
		updateState()
	}
	
	private func unbind(_ textField: UITextField? ) {
		guard let textField = textField else { return }
		textField.delegate = nil
		textField.removeTarget(self, action: #selector(updateState), for: .editingChanged)
	}
	
	@objc private func updateState() {
		var nextState: State = regularExpression.map { $0.hasMatch(in: passwordField) ? .validPassword : [] } ?? []
		if passwordField?.text == passwordConfirmationField?.text && nextState.contains(.validPassword) {
			nextState.formUnion(.confirmedPassword)
		}
		state = nextState
		setTextField(passwordField, rightViewWithImage: state.contains(.validPassword) ? successImage: failureImage)
		setTextField(
			passwordConfirmationField,
			rightViewWithImage: state.contains(.confirmedPassword) ? successImage: failureImage
		)
	}
	
	private func setTextField(_ textField: UITextField?, rightViewWithImage image: UIImage?) {
		guard let textField = textField, let image = image else { return }
		let imageView = UIImageView(image: image)
		imageView.tintColor = tintColor
		textField.rightView = imageView
		textField.rightViewMode = .always
	}
}

extension PasswordControl: UITextFieldDelegate {
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		switch (textField, isPasswordValid, isPasswordConfirmed) {
		case (passwordField, true, _):
			delegate?.passwordControlPasswordFieldDidReturn?(self)
			passwordConfirmationField?.becomeFirstResponder()
			return true
		case (passwordField, false, _):
			delegate?.passwordControlPasswordFieldDidFailToReturn?(self)
			return false
		case (passwordConfirmationField, true, true):
			delegate?.passwordControlPasswordConfirmationFieldDidReturn?(self)
			return true
		case (passwordConfirmationField, _, false):
			delegate?.passwordControlPasswordConfirmationFieldDidFailToReturn?(self)
			return false
		default:
			// Should never happen
			return false
		}
	}
}
