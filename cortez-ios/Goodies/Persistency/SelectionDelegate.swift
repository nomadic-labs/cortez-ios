// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

protocol SelectionDelegate {
	
	associatedtype SectionDelegateElement
	
	func selectionWillChange<T: Selection>(_ selection: T) where SectionDelegateElement == T.SectionElement
	func selection<T: Selection, U: Section>(_ selection: T, didInsert section: U, at index: Int) where SectionDelegateElement == T.SectionElement, SectionDelegateElement == U.Element
	func selection<T: Selection, U: Section>(_ selection: T, didDelete section: U, at index: Int) where SectionDelegateElement == T.SectionElement, SectionDelegateElement == U.Element
	func selection<T: Selection>(_ selection: T, didInsert element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement
	func selection<T: Selection>(_ selection: T, didDelete element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement
	func selection<T: Selection>(_ selection: T, didUpdate element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement
	func selection<T: Selection>(_ selection: T, didMove element: SectionDelegateElement, from indexPath: IndexPath, to newIndexPath: IndexPath) where SectionDelegateElement == T.SectionElement
	func selectionDidChange<T: Selection>(_ selection: T) where SectionDelegateElement == T.SectionElement
}

// everything is optional
extension SelectionDelegate {
	func selectionWillChange<T: Selection>(_ selection: T) where SectionDelegateElement == T.SectionElement {}
	func selection<T: Selection, U: Section>(_ selection: T, didInsert section: U, at index: Int) where SectionDelegateElement == T.SectionElement, SectionDelegateElement == U.Element {}
	func selection<T: Selection, U: Section>(_ selection: T, didDelete section: U, at index: Int) where SectionDelegateElement == T.SectionElement, SectionDelegateElement == U.Element {}
	func selection<T: Selection>(_ selection: T, didInsert element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement {}
	func selection<T: Selection>(_ selection: T, didDelete element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement {}
	func selection<T: Selection>(_ selection: T, didUpdate element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement {}
	func selection<T: Selection>(_ selection: T, didMove element: SectionDelegateElement, from indexPath: IndexPath, to newIndexPath: IndexPath) where SectionDelegateElement == T.SectionElement {}
	func selectionDidChange<T: Selection>(_ selection: T) where SectionDelegateElement == T.SectionElement {}
}

class SelectionDelegateBase<SectionElement>: SelectionDelegate {
	
	func selectionWillChange<T: Selection>(_ selection: T) where SectionElement == T.SectionElement {
		fatalError()
	}
	func selection<T: Selection, U: Section>(_ selection: T, didInsert section: U, at index: Int) where SectionElement == T.SectionElement, SectionElement == U.Element {
		fatalError()
	}
	func selection<T: Selection, U: Section>(_ selection: T, didDelete section: U, at index: Int) where SectionElement == T.SectionElement, SectionElement == U.Element {
		fatalError()
	}
	func selection<T: Selection>(_ selection: T, didInsert element: SectionElement, at indexPath: IndexPath) where SectionElement == T.SectionElement {
		fatalError()
	}
	func selection<T: Selection>(_ selection: T, didDelete element: SectionElement, at indexPath: IndexPath) where SectionElement == T.SectionElement {
		fatalError()
	}
	func selection<T: Selection>(_ selection: T, didUpdate element: SectionElement, at indexPath: IndexPath) where SectionElement == T.SectionElement {
		fatalError()
	}
	func selection<T: Selection>(_ selection: T, didMove element: SectionElement, from indexPath: IndexPath, to newIndexPath: IndexPath) where SectionElement == T.SectionElement {
		fatalError()
	}
	func selectionDidChange<T: Selection>(_ selection: T) where SectionElement == T.SectionElement {
		fatalError()
	}
}

final class SelectionDelegateBox<Base: SelectionDelegate>: SelectionDelegateBase<Base.SectionDelegateElement> {
	
	private let base: Base
	
	init(_ base: Base) {
		self.base = base
	}
	
	override func selectionWillChange<T: Selection>(_ selection: T) where SectionDelegateElement == T.SectionElement {
		base.selectionWillChange(selection)
	}
	override func selection<T: Selection, U: Section>(_ selection: T, didInsert section: U, at index: Int) where SectionDelegateElement == T.SectionElement, SectionDelegateElement == U.Element {
		base.selection(selection, didInsert: section, at: index)
	}
	override func selection<T: Selection, U: Section>(_ selection: T, didDelete section: U, at index: Int) where SectionDelegateElement == T.SectionElement, SectionDelegateElement == U.Element {
		base.selection(selection, didDelete: section, at: index)
	}
	override func selection<T: Selection>(_ selection: T, didInsert element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement {
		base.selection(selection, didInsert: element, at: indexPath)
	}
	override func selection<T: Selection>(_ selection: T, didDelete element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement {
		base.selection(selection, didDelete: element, at: indexPath)
	}
	override func selection<T: Selection>(_ selection: T, didUpdate element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement {
		base.selection(selection, didUpdate: element, at: indexPath)
	}
	override func selection<T: Selection>(_ selection: T, didMove element: SectionDelegateElement, from indexPath: IndexPath, to newIndexPath: IndexPath) where SectionDelegateElement == T.SectionElement {
		base.selection(selection, didMove: element, from: indexPath, to: newIndexPath)
	}
	override func selectionDidChange<T: Selection>(_ selection: T) where SectionDelegateElement == T.SectionElement {
		base.selectionDidChange(selection)
	}
}

final class SelectionDelegateWeakBox<Base: SelectionDelegate & AnyObject>: SelectionDelegateBase<Base.SectionDelegateElement> {
	
	private weak var base: Base?
	
	init(_ base: Base) {
		self.base = base
	}
	
	override func selectionWillChange<T: Selection>(_ selection: T) where SectionDelegateElement == T.SectionElement {
		base?.selectionWillChange(selection)
	}
	override func selection<T: Selection, U: Section>(_ selection: T, didInsert section: U, at index: Int) where SectionDelegateElement == T.SectionElement, SectionDelegateElement == U.Element {
		base?.selection(selection, didInsert: section, at: index)
	}
	override func selection<T: Selection, U: Section>(_ selection: T, didDelete section: U, at index: Int) where SectionDelegateElement == T.SectionElement, SectionDelegateElement == U.Element {
		base?.selection(selection, didDelete: section, at: index)
	}
	override func selection<T: Selection>(_ selection: T, didInsert element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement {
		base?.selection(selection, didInsert: element, at: indexPath)
	}
	override func selection<T: Selection>(_ selection: T, didDelete element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement {
		base?.selection(selection, didDelete: element, at: indexPath)
	}
	override func selection<T: Selection>(_ selection: T, didUpdate element: SectionDelegateElement, at indexPath: IndexPath) where SectionDelegateElement == T.SectionElement {
		base?.selection(selection, didUpdate: element, at: indexPath)
	}
	override func selection<T: Selection>(_ selection: T, didMove element: SectionDelegateElement, from indexPath: IndexPath, to newIndexPath: IndexPath) where SectionDelegateElement == T.SectionElement {
		base?.selection(selection, didMove: element, from: indexPath, to: newIndexPath)
	}
	override func selectionDidChange<T: Selection>(_ selection: T) where SectionDelegateElement == T.SectionElement {
		base?.selectionDidChange(selection)
	}
}

struct AnySelectionDelegate<SectionElement>: SelectionDelegate {
	
	private let base: SelectionDelegateBase<SectionElement>
	
	init(_ base: SelectionDelegateBase<SectionElement>) {
		self.base = base
	}
	init<T: SelectionDelegate>(_ base: T) where SectionElement == T.SectionDelegateElement {
		self.init(SelectionDelegateBox(base))
	}

	func selectionWillChange<T: Selection>(_ selection: T) where SectionElement == T.SectionElement {
		base.selectionWillChange(selection)
	}
	func selection<T: Selection, U: Section>(_ selection: T, didInsert section: U, at index: Int) where SectionElement == T.SectionElement, SectionElement == U.Element {
		base.selection(selection, didInsert: section, at: index)
	}
	func selection<T: Selection, U: Section>(_ selection: T, didDelete section: U, at index: Int) where SectionElement == T.SectionElement, SectionElement == U.Element {
		base.selection(selection, didDelete: section, at: index)
	}
	func selection<T: Selection>(_ selection: T, didInsert element: SectionElement, at indexPath: IndexPath) where SectionElement == T.SectionElement {
		base.selection(selection, didInsert: element, at: indexPath)
	}
	func selection<T: Selection>(_ selection: T, didDelete element: SectionElement, at indexPath: IndexPath) where SectionElement == T.SectionElement {
		base.selection(selection, didDelete: element, at: indexPath)
	}
	func selection<T: Selection>(_ selection: T, didUpdate element: SectionElement, at indexPath: IndexPath) where SectionElement == T.SectionElement {
		base.selection(selection, didUpdate: element, at: indexPath)
	}
	func selection<T: Selection>(_ selection: T, didMove element: SectionElement, from indexPath: IndexPath, to newIndexPath: IndexPath) where SectionElement == T.SectionElement {
		base.selection(selection, didMove: element, from: indexPath, to: newIndexPath)
	}
	func selectionDidChange<T: Selection>(_ selection: T) where SectionElement == T.SectionElement {
		base.selectionDidChange(selection)
	}
}
