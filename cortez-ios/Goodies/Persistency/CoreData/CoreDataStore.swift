// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import CoreData

protocol DTOEncoder {

	associatedtype DTO: NSManagedObject

	func encode(to dto: DTO)
}

protocol DTODecoder {
	
	associatedtype DTO: NSManagedObject

	init(from dto: DTO)
}

protocol FetchRequestConvertible {
	
	associatedtype DTO: NSManagedObject
	
	var request: NSFetchRequest<DTO> { get }
	
	// optional
	func fetchedResultsController(with context: NSManagedObjectContext) -> NSFetchedResultsController<DTO>
}

extension FetchRequestConvertible {
	func fetchedResultsController(with context: NSManagedObjectContext) -> NSFetchedResultsController<DTO> {
		let request = self.request
		
		return NSFetchedResultsController(
			fetchRequest: request,
			managedObjectContext: context,
			sectionNameKeyPath: request.sortDescriptors.filter { $0.count > 1 }?.first?.key,
			cacheName: nil
		)
	}
}

extension Query: FetchRequestConvertible where Element.Predicate: FetchRequestConvertible {

	var request: NSFetchRequest<Element.Predicate.DTO> {
		let request = predicate.request
		request.sortDescriptors = sortDescs.map { NSSortDescriptor(key: $0.attribute.name, ascending: $0.ascending) }
		return request
	}
}

typealias CoreDataBacked = FetchRequestConvertible & DTOEncoder & DTODecoder

final class CoreDataStore<Element: Storable & CoreDataBacked>: Store where Element.Predicate: FetchRequestConvertible,
Element.DTO == Element.Predicate.DTO {

	enum Error: Swift.Error {
		case unknown
		case fileNotFound(String)
		case coreData(Swift.Error)
	}

	private let container: NSPersistentContainer
	
	init(_ container: NSPersistentContainer) {
		self.container = container
		loadPersistentStores()
	}
	deinit {
	}
	
	convenience init(_ name: String) throws {
/*
		guard let storeURL = FileManager.default.documentURL?.appendingPathComponent(name.lowercased()) else { throw Error.unknown }
		guard let modelURL = Bundle.main.url(forResource: name, withExtension: "momd") else { throw Error.unknown }
		guard let model = NSManagedObjectModel(contentsOf: modelURL) else { throw Error.unknown }

		let storeCoordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
		try storeCoordinator.addPersistentStore(
			ofType: NSSQLiteStoreType,
			configurationName: nil,
			at: storeURL,
			options: [
				NSInferMappingModelAutomaticallyOption: true,
				NSMigratePersistentStoresAutomaticallyOption: true
			]
		)
*/
		self.init(NSPersistentContainer(name: name))
	}

	func select(_ query: Query<Element>) -> AnySelection<Element> {
		return AnySelection(CoreDataSelection(query.fetchedResultsController(with: container.viewContext)))
	}

	func enqueue<T: Transaction>(transaction: T, completion: TransactionCompletion?) where Element == T.Element {
		container.performBackgroundTask { context in
			do {
				try transaction.update(CoreDataContext(context))
				if context.hasChanges {
					try context.save()
				}
				DispatchQueue.main.async { completion?(nil) }
			} catch {
				DispatchQueue.main.async { completion?(error) }
			}
		}
	}
	
	func deleteAll() {
		container.viewContext.reset()
		let persistentStores = container.persistentStoreCoordinator.persistentStores
		persistentStores.forEach { $0.destroy() }
		loadPersistentStores()
	}

	private func loadPersistentStores() {
		container.loadPersistentStores { [unowned container] description, error in
			if let error = error {
				debug(error)
				return
			}
			debug(description.url ?? "")
			container.viewContext.automaticallyMergesChangesFromParent = true
			// TODO: handle correctly lazy loading
		}
	}
}

extension NSPersistentStore {
	func destroy() {
		guard let storeCoordinator = persistentStoreCoordinator else { return }
		guard let url = url else { return }
		do {
			try storeCoordinator.destroyPersistentStore(at: url, ofType: type, options: options)
			//try storeCoordinator.remove(self)
			//try? FileManager.default.removeItem(at: url)
			//try storeCoordinator.addPersistentStore(ofType: type, configurationName: nil, at: url, options: options)
		} catch {
			debug(error)
		}
	}
}
