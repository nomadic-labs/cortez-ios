// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import CoreData

final class CoreDataSelection<Element: DTODecoder>: NSObject, Selection, NSFetchedResultsControllerDelegate {
	
	let fetchedResultsController: NSFetchedResultsController<Element.DTO>
	
	var delegate: AnySelectionDelegate<Element>?
	
	var indexTitles: [String]? {
		return fetchedResultsController.sectionIndexTitles
	}
	var count: Int {
		return fetchedResultsController.sections?.count ?? 0
	}
	
	init(_ fetchedResultsController: NSFetchedResultsController<Element.DTO>) {
		self.fetchedResultsController = fetchedResultsController
		super.init()
		fetchedResultsController.delegate = self
		try? fetchedResultsController.performFetch()
	}

	subscript(index: Int) -> AnySection<Element> {
		guard let sections = fetchedResultsController.sections else { fatalError() }
		return AnySection(CoreDataSection(sections[index]))
	}

	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		delegate?.selectionWillChange(self)
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
		guard let delegate = delegate else { return }
		let section = CoreDataSection<Element>(sectionInfo)
		switch type {
		case .insert:
			delegate.selection(self, didInsert: section, at: sectionIndex)
		case .delete:
			delegate.selection(self, didDelete: section, at: sectionIndex)
		default: // ignore
			break
		}
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		guard let delegate = delegate else { return }
		guard let element = (anObject as? Element.DTO).map(Element.init) else { return }
		switch type {
		case .insert:
			guard let newIndexPath = newIndexPath else { return }
			delegate.selection(self, didInsert: element, at: newIndexPath)
		case .delete:
			guard let indexPath = indexPath else { return }
			delegate.selection(self, didDelete: element, at: indexPath)
		case .update:
			guard let indexPath = indexPath else { return }
			delegate.selection(self, didUpdate: element, at: indexPath)
		case .move:
			guard let indexPath = indexPath else { return }
			guard let newIndexPath = newIndexPath else { return }
			/*
			A move is reported when the changed attribute on the object is one of the sort descriptors used in the fetch request.
			An update of the object is assumed in this case, but no separate update message is sent to the delegate.
			*/
			if indexPath == newIndexPath {
				delegate.selection(self, didUpdate: element, at: indexPath)
			} else {
				delegate.selection(self, didMove: element, from: indexPath, to: newIndexPath)
			}
		@unknown default:
			break
		}
	}
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		delegate?.selectionDidChange(self)
	}
}
