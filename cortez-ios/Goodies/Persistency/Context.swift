// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

protocol Context {
	
	associatedtype Element: Storable
	
	func select(_ query: Query<Element>) throws -> [Element]
	
	func insert(_ element: Element) throws
	func update(_ element: Element) throws
	func delete(_ element: Element) throws
}

class ContextBase<Element: Storable>: Context {
	
	func select(_ query: Query<Element>) throws -> [Element] { fatalError() }
	
	func insert(_ element: Element) throws { fatalError() }
	func update(_ element: Element) throws { fatalError() }
	func delete(_ element: Element) throws { fatalError() }
}

final class ContextBox<Base: Context>: ContextBase<Base.Element> {
	
	private let base: Base
	
	init(_ base: Base) {
		self.base = base
	}
	
	override func select(_ query: Query<Element>) throws -> [Element] {
		return try base.select(query)
	}
	
	override func insert(_ element: Element) throws {
		try base.insert(element)
	}
	override func update(_ element: Element) throws {
		try base.update(element)
	}
	override func delete(_ element: Element) throws {
		try base.delete(element)
	}
}

struct AnyContext<Element: Storable>: Context {
	
	private let base: ContextBase<Element>
	
	init<T: Context>(_ base: T) where Element == T.Element {
		self.base = ContextBox(base)
	}
	
	func select(_ query: Query<Element>) throws -> [Element] {
		return try base.select(query)
	}
	
	func insert(_ element: Element) throws {
		try base.insert(element)
	}
	func update(_ element: Element) throws {
		try base.update(element)
	}
	func delete(_ element: Element) throws {
		try base.delete(element)
	}
}
