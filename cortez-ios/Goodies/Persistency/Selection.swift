// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

protocol Selection {

	associatedtype SectionElement

	var delegate: AnySelectionDelegate<SectionElement>? { get set }

	var indexTitles: [String]? { get }
	var count: Int { get }
	
	subscript (index: Int) -> AnySection<SectionElement> { get }
}

extension Selection {
	var isEmpty: Bool {
		return count == 0
	}
	var first: AnySection<SectionElement>? {
		return isEmpty ? nil : self[0]
	}
	subscript (indexPath: IndexPath) -> SectionElement {
		return self[indexPath.section][indexPath.row]
	}
}

class SelectionBase<Element>: Selection {
	
	var delegate: AnySelectionDelegate<Element>? {
		get {
			fatalError()
		}
		set {
			fatalError()
		}
	}
	
	var indexTitles: [String]? { fatalError() }
	var count: Int { fatalError() }
	
	subscript(index: Int) -> AnySection<Element> {
		fatalError()
	}
}

final class SelectionBox<Base: Selection>: SelectionBase<Base.SectionElement> {
	
	override var delegate: AnySelectionDelegate<SectionElement>? {
		get {
			return base.delegate
		}
		set {
			base.delegate = newValue
		}
	}
	
	private var base: Base
	
	override var indexTitles: [String]? {
		return base.indexTitles
	}
	override var count: Int {
		return base.count
	}
	
	init(_ base: Base) {
		self.base = base
	}
	
	override subscript(index: Int) -> AnySection<SectionElement> {
		return base[index]
	}
}

struct AnySelection<SectionElement>: Selection {
	
	private let base: SelectionBase<SectionElement>
	
	var delegate: AnySelectionDelegate<SectionElement>? {
		get {
			return base.delegate
		}
		set {
			base.delegate = newValue
		}
	}
	
	var indexTitles: [String]? {
		return base.indexTitles
	}
	var count: Int {
		return base.count
	}
	
	init<T: Selection>(_ base: T) where SectionElement == T.SectionElement {
		self.base = SelectionBox(base)
	}
	
	subscript(index: Int) -> AnySection<SectionElement> {
		return base[index]
	}
}

struct SelectionIterator<SectionElement>: IteratorProtocol {
	
	private var index: Int = -1
	private let selection: AnySelection<SectionElement>

	init(_ selection: AnySelection<SectionElement>) {
		self.selection = selection
	}
	init<T: Selection>(_ selection: T) where SectionElement == T.SectionElement {
		self.init(AnySelection(selection))
	}

	mutating func next() -> AnySection<SectionElement>? {
		index += 1
		return index < selection.count ? selection[index] : nil
	}
}

extension AnySelection: Sequence {
	func makeIterator() -> SelectionIterator<SectionElement> {
		return SelectionIterator(self)
	}
}
