// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

final class MappedSelection<Base: Selection, SectionElement>: SelectionDelegateBase<Base.SectionElement>, Selection {
	
	var delegate: AnySelectionDelegate<SectionElement>? {
		didSet {
			base.delegate = delegate.map { _ in AnySelectionDelegate(SelectionDelegateWeakBox(self)) }
		}
	}
	
	var indexTitles: [String]? {
		return base.indexTitles
	}
	var count: Int {
		return base.count
	}
	
	var base: Base
	let transform: (IndexPath, Base.SectionElement) -> SectionElement
	
	init(base: Base, transform: @escaping (IndexPath, Base.SectionElement) -> SectionElement) {
		self.base = base
		self.transform = transform
	}
	
	subscript(index: Int) -> AnySection<SectionElement> {
		return base[index].map(index: index, transform: transform)
	}
	
	override func selectionWillChange<T: Selection>(_ selection: T) where Base.SectionElement == T.SectionElement {
		delegate?.selectionWillChange(self)
	}
	override func selection<T: Selection, U: Section>(_ selection: T, didInsert section: U, at index: Int) where Base.SectionElement == T.SectionElement, Base.SectionElement == U.Element {
		delegate?.selection(self, didInsert: section.map(index: index, transform: transform), at: index)
	}
	override func selection<T: Selection, U: Section>(_ selection: T, didDelete section: U, at index: Int) where Base.SectionElement == T.SectionElement, Base.SectionElement == U.Element {
		delegate?.selection(self, didDelete: section.map(index: index, transform: transform), at: index)
	}
	override func selection<T: Selection>(_ selection: T, didInsert element: Base.SectionElement, at indexPath: IndexPath) where Base.SectionElement == T.SectionElement {
		delegate?.selection(self, didInsert: transform(indexPath, element), at: indexPath)
	}
	override func selection<T: Selection>(_ selection: T, didDelete element: Base.SectionElement, at indexPath: IndexPath) where Base.SectionElement == T.SectionElement {
		delegate?.selection(self, didDelete: transform(indexPath, element), at: indexPath)
	}
	override func selection<T: Selection>(_ selection: T, didUpdate element: Base.SectionElement, at indexPath: IndexPath) where Base.SectionElement == T.SectionElement {
		delegate?.selection(self, didUpdate: transform(indexPath, element), at: indexPath)
	}
	override func selection<T: Selection>(_ selection: T, didMove element: Base.SectionElement, from indexPath: IndexPath, to newIndexPath: IndexPath) where Base.SectionElement == T.SectionElement {
		delegate?.selection(self, didMove: transform(newIndexPath, element), from: indexPath, to: newIndexPath)
	}
	override func selectionDidChange<T: Selection>(_ selection: T) where Base.SectionElement == T.SectionElement {
		delegate?.selectionDidChange(self)
	}
}

extension Selection {
	func map<T>(_ transform: @escaping (IndexPath, SectionElement) -> T) -> AnySelection<T> {
		return AnySelection<T>(MappedSelection(base: self, transform: transform))
	}
}
