// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

import UIKit

protocol Named {
	var name: String { get }
}

protocol Storable {
	associatedtype Predicate
	associatedtype Attribute: Named
}

struct Query<Element: Storable> {
	
	typealias SortDesc = (attribute: Element.Attribute, ascending: Bool)

	let predicate: Element.Predicate
	let sortDescs: [SortDesc]
	
	static func `where`(_ predicate: Element.Predicate) -> Query {
		Query(predicate: predicate, sortDescs: [])
	}
	
	func sorted(by attribute: Element.Attribute, ascending: Bool) -> Query {
		Query(predicate: predicate, sortDescs: sortDescs + CollectionOfOne(SortDesc(attribute, ascending)))
	}
}

protocol Store {

	associatedtype Element: Storable

	typealias TransactionCompletion = (Error?) -> Void

	func select(_ query: Query<Element>) -> AnySelection<Element>
	func enqueue<T: Transaction>(transaction: T, completion: TransactionCompletion?) where Element == T.Element
	func deleteAll()
}

private func bind<T>(_ element: T) -> (@escaping (Result<T, Error>) -> Void) -> (Error?) -> Void {
	return { completion in
		{ error in
			if let error = error {
				completion(.failure(error))
				return
			}
			completion(.success(element))
		}
	}
}

extension Store {
	
	typealias Completion = (Result<Element, Error>) -> Void
	
	func enqueue<T: Transaction>(transaction: T) where Element == T.Element {
		enqueue(transaction: transaction, completion: nil)
	}
	func enqueue(transaction body: @escaping AnyTransaction<Element>.Body) {
		enqueue(transaction: AnyTransaction(body), completion: nil)
	}
	func enqueue(transaction body: @escaping AnyTransaction<Element>.Body, completion: TransactionCompletion?) {
		enqueue(transaction: AnyTransaction(body), completion: completion)
	}
	
	func insert(_ element: Element, completion: Completion? = nil) {
		enqueue(transaction: { try $0.insert(element) }, completion: completion.map(bind(element)))
	}
	func update(_ element: Element, completion: Completion? = nil) {
		enqueue(transaction: { try $0.update(element) }, completion: completion.map(bind(element)))
	}
	func delete(_ element: Element, completion: Completion? = nil) {
		enqueue(transaction: { try $0.delete(element) }, completion: completion.map(bind(element)))
	}
}

class StoreBase<Element: Storable>: Store {
	func select(_ query: Query<Element>) -> AnySelection<Element> {
		fatalError()
	}
	func enqueue<T: Transaction>(transaction: T, completion: TransactionCompletion?) where Element == T.Element {
		fatalError()
	}
	func deleteAll() {
		fatalError()
	}
}

final class StoreBox<Base: Store>: StoreBase<Base.Element> {
	
	private let base: Base
	
	init(_ base: Base) {
		self.base = base
	}
	
	override func select(_ query: Query<Element>) -> AnySelection<Element> {
		return base.select(query)
	}
	override func enqueue<T: Transaction>(transaction: T, completion: TransactionCompletion?) where Element == T.Element {
		base.enqueue(transaction: transaction, completion: completion)
	}
	override func deleteAll() {
		base.deleteAll()
	}
}

struct AnyStore<Element: Storable>: Store {
	
	private let base: StoreBase<Element>
	
	init<T: Store>(_ base: T) where Element == T.Element {
		self.base = StoreBox(base)
	}
	
	func select(_ query: Query<Element>) -> AnySelection<Element> {
		return base.select(query)
	}
	func enqueue<T: Transaction>(transaction: T, completion: TransactionCompletion?) where Element == T.Element {
		base.enqueue(transaction: transaction, completion: completion)
	}
	func deleteAll() {
		base.deleteAll()
	}
}
