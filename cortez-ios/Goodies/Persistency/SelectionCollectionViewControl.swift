// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

final class SelectionCollectionViewControl<SectionElement>: SelectionDelegate {

	var inserts = [IndexPath]()
	var deletes = [IndexPath]()
	var updates = [IndexPath]()

	var moves = [(IndexPath, IndexPath)]()
	
	weak var collectionView: UICollectionView!

	init(collectionView: UICollectionView) {
		self.collectionView = collectionView
	}

	func selectionWillChange<T: Selection>(_ selection: T) where SectionElement == T.SectionElement {
	}
	
	func selection<T: Selection, U: Section>(_ selection: T, didInsert section: U, at index: Int) where SectionElement == T.SectionElement, SectionElement == U.Element {
	}
	
	func selection<T: Selection, U: Section>(_ selection: T, didDelete section: U, at index: Int) where SectionElement == T.SectionElement, SectionElement == U.Element {
	}
	
	func selection<T: Selection>(_ selection: T, didInsert element: SectionElement, at indexPath: IndexPath) where SectionElement == T.SectionElement {
		inserts.append(indexPath)
	}
	
	func selection<T: Selection>(_ selection: T, didDelete element: SectionElement, at indexPath: IndexPath) where SectionElement == T.SectionElement {
		deletes.append(indexPath)
	}
	
	func selection<T: Selection>(_ selection: T, didUpdate element: SectionElement, at indexPath: IndexPath) where SectionElement == T.SectionElement {
		updates.append(indexPath)
	}
	
	func selection<T: Selection>(_ selection: T, didMove element: SectionElement, from indexPath: IndexPath, to newIndexPath: IndexPath) where SectionElement == T.SectionElement {
		moves.append((indexPath, newIndexPath))
	}
	
	func selectionDidChange<T: Selection>(_ selection: T) where SectionElement == T.SectionElement {
		collectionView.performBatchUpdates({
			collectionView.insertItems(at: inserts)
			collectionView.deleteItems(at: deletes)
			collectionView.reloadItems(at: updates)
			moves.forEach {
				collectionView.deleteItems(at: [$0])
				collectionView.insertItems(at: [$1])
				//collectionView.moveItem(at: $0, to: $1)
			}
			self.inserts.removeAll()
			self.deletes.removeAll()
			self.updates.removeAll()
			self.moves.removeAll()
		}, completion: nil)
	}
}
