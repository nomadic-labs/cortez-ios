// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

protocol Section {

	associatedtype Element
	
	var indexTitle: String? { get }
	var count: Int { get }
	
	subscript (index: Int) -> Element { get }
}

extension Section {
	var isEmpty: Bool {
		return count == 0
	}
	var first: Element? {
		return isEmpty ? nil : self[0]
	}
}

class SectionBase<Element>: Section {
	
	var indexTitle: String? { fatalError() }
	var count: Int { fatalError() }
	
	subscript(index: Int) -> Element {
		fatalError()
	}
}

final class SectionBox<Base: Section>: SectionBase<Base.Element> {
	
	private let base: Base
	
	override var indexTitle: String? {
		return base.indexTitle
	}
	override var count: Int {
		return base.count
	}
	
	init(_ base: Base) {
		self.base = base
	}
	
	override subscript(index: Int) -> Element {
		return base[index]
	}
}

final class MappedSection<Base: Section, Element>: SectionBase<Element> {
	
	typealias Transform = (IndexPath, Base.Element) -> Element
	
	private let base: Base
	private let index: Int
	private let transform: Transform
	
	override var indexTitle: String? {
		return base.indexTitle
	}
	override var count: Int {
		return base.count
	}
	
	init(base: Base, index: Int, transform: @escaping Transform) {
		self.base = base
		self.index = index
		self.transform = transform
	}
	
	override subscript(index: Int) -> Element {
		return transform(IndexPath(row: index, section: self.index), base[index])
	}
}

extension Section {
	func map<T>(index: Int, transform: @escaping (IndexPath, Element) -> T) -> AnySection<T> {
		let mappedSection: SectionBase<T> = MappedSection(base: self, index: index, transform: transform)
		return AnySection(mappedSection)
	}
}

struct AnySection<Element>: Section {
	
	private let base: SectionBase<Element>
	
	var indexTitle: String? {
		return base.indexTitle
	}
	var count: Int {
		return base.count
	}

	init(_ base: SectionBase<Element>) {
		self.base = base
	}
	init<T: Section>(_ base: T) where Element == T.Element {
		self.init(SectionBox(base))
	}
	
	subscript(index: Int) -> Element {
		return base[index]
	}
}

struct SectionIterator<Element>: IteratorProtocol {
	
	private var index: Int = -1
	private let section: AnySection<Element>
	
	init(_ section: AnySection<Element>) {
		self.section = section
	}
	init<T: Section>(_ section: T) where Element == T.Element {
		self.init(AnySection(section))
	}
	
	mutating func next() -> Element? {
		index += 1
		return index < section.count ? section[index] : nil
	}
}

extension AnySection: Sequence {
	func makeIterator() -> SectionIterator<Element> {
		return SectionIterator(self)
	}
}
