// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************


import Foundation

struct Response {
	let urlResponse: URLResponse?
	let body: Data?
}

final class Call {

	enum Error: Swift.Error {
		
		enum Parser: Swift.Error {
			case invalidDataLength
		}

		case unknown
		case http(Int)
		case session(Swift.Error)
		case parsing(Swift.Error)
		case alreadyExecuted
	}
	
	private var task: URLSessionTask?
	private var callback: AnyCallback<Response, Error>?
	
	init(request: URLRequest, session: URLSession = .shared) {
		task = session.dataTask(with: request) { [weak self] in
			self?.handle(data: $0, response: $1, error: $2)
		}
		
	}
	deinit {
		task?.cancel()
	}

	func enqueue<T: Callback>(_ callback: T) where Response == T.Value, Error == T.Error {
		guard let task = task else {
			callback.on(.unknown)
			return
		}
		guard task.state == .suspended && self.callback == nil else {
			callback.on(.alreadyExecuted)
			return
		}

		self.callback = AnyCallback(callback)
		#if DEBUG
		if let request = task.originalRequest {
			debug("\(request)")
			if let body = request.httpBody.flatMap({ String(data: $0, encoding: .utf8) }) {
				debug("body: \(body)")
			}
		}
		#endif
		task.resume()
	}
	
	func cancel() {
		task?.cancel()
	}
	
	private func handle(data: Data?, response: URLResponse?, error: Swift.Error?) {
		guard let callback = callback else { return } // should not happen
		if let error = error {
			callback.on(.session(error))
			return
		}
		if let response = response as? HTTPURLResponse, response.statusCode >= 300 {
			callback.on(.http(response.statusCode))
			return
		}
		callback.on(Response(urlResponse: response, body: data))
	}
}

extension Call {
	func enqueue(callback body: @escaping AnyCallback<Response, Error>.Body) {
		enqueue(AnyCallback(body))
	}
}

extension Call {

	typealias OnError = (Error) -> Void

	func enqeue<T: Decodable>(jsonDecoder: JSONDecoder = .init(), on: @escaping (Result<T, Call.Error>) -> Void) {
		enqueue(AnyCallback(jsonDecoder: jsonDecoder, body: on))
	}
	func enqeue<T: Decodable  & ExpressibleByDescription>(jsonDecoder: JSONDecoder = .init(), on: @escaping (Result<T, Call.Error>) -> Void) {
		enqueue(AnyCallback(jsonDecoder: jsonDecoder, body: on))
	}

	func enqueue<T: Decodable>(jsonDecoder: JSONDecoder = .init(), onValue: @escaping (T) -> Void, onError: @escaping OnError) {
		enqueue(AnyCallback(jsonDecoder: jsonDecoder, onValue: onValue, onError: onError))
	}
	func enqueue<T: Decodable & ExpressibleByDescription>(jsonDecoder: JSONDecoder = .init(), onValue: @escaping (T) -> Void, onError: @escaping OnError) {
		enqueue(AnyCallback(jsonDecoder: jsonDecoder, onValue: onValue, onError: onError))
	}
}
