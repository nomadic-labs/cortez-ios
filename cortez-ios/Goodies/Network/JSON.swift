// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

enum SR6163: Swift.Error {
	case invalidFormat
}

protocol ExpressibleByDescription {
	init?(_ description: String)
}

extension Int: ExpressibleByDescription {}
extension Int64: ExpressibleByDescription {}
extension UInt: ExpressibleByDescription {}
extension UInt64: ExpressibleByDescription {}

extension String: ExpressibleByDescription {}

extension AnyCallback where Value == Response, Error == Call.Error {
	
	typealias OnError = (Call.Error) -> Void
	
	init<T: Decodable>(jsonDecoder: JSONDecoder, body: @escaping (Result<T, Call.Error>) -> Void) {
		self.init { result in
			switch result {
			case .success(let response):
				do {
					guard let data = response.body else { throw Error.Parser.invalidDataLength }
					body(.success(try jsonDecoder.decode(T.self, from: data)))
				} catch { // https://bugs.swift.org/browse/SR-6163
					body(.failure(.parsing(error)))
				}
			case .failure(let error):
				body(.failure(error))
			}
		}
	}
	
	init<T: Decodable & ExpressibleByDescription>(jsonDecoder: JSONDecoder, body: @escaping (Result<T, Call.Error>) -> Void) {
		self.init { result in
			switch result {
			case .success(let response):
				do {
					guard let data = response.body else { throw SR6163.invalidFormat }
					let characterSet = CharacterSet.whitespacesAndNewlines.updating(with: "\"")
					guard let value = (String(data: data, encoding: .utf8)?.trimmingCharacters(in: characterSet)).flatMap(T.init) else {
						throw SR6163.invalidFormat
					}
					body(.success(value))
				} catch { // https://bugs.swift.org/browse/SR-6163
					body(.failure(.parsing(error)))
				}
			case .failure(let error):
				body(.failure(error))
			}
		}
	}

	init<T: Decodable>(jsonDecoder: JSONDecoder, onValue: @escaping (T) -> Void, onError: @escaping OnError) {
		self.init(jsonDecoder: jsonDecoder) { (result: Result<T, Call.Error>) in
			switch result {
			case .success(let value): onValue(value)
			case .failure(let error): onError(error)
			}
		}
	}
	
	init<T: Decodable & ExpressibleByDescription>(jsonDecoder: JSONDecoder, onValue: @escaping (T) -> Void, onError: @escaping OnError) {
		self.init(jsonDecoder: jsonDecoder) { (result: Result<T, Call.Error>) in
			switch result {
			case .success(let value): onValue(value)
			case .failure(let error): onError(error)
			}
		}
	}
}
