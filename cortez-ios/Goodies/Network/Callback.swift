// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

protocol Callback {
	
	associatedtype Value
	associatedtype Error: Swift.Error

	func on(_ result: Result<Value, Error>)
}

extension Callback {
	func on(_ value: Value) {
		on(.success(value))
	}
	func on(_ error: Error) {
		on(.failure(error))
	}
}

struct AnyCallback<Value, Error>: Callback where Error: Swift.Error {

	typealias Body = (Result<Value, Error>) -> Void
	
	private let body: Body
	
	init(_ body: @escaping Body) {
		self.body = body
	}

	func on(_ result: Result<Value, Error>) {
		body(result)
	}
}

extension AnyCallback {
	init<T: Callback>(_ base: T) where Value == T.Value, AnyCallback.Error == T.Error {
		self.init(base.on)
	}
}
