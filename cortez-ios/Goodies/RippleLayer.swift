// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

let expandRippleBeyondSurface: CGFloat = 10
let rippleStartingScale: CGFloat = 0.6

let rippleTouchDownDuration: TimeInterval = 0.225
let rippleTouchUpDuration: TimeInterval = 0.15

let rippleFadeInDuration: TimeInterval = 0.075
let rippleFadeOutDuration: TimeInterval = 0.075
let rippleFadeOutDelay: TimeInterval = 0.15

let rippleLayerOpacityKeyPath = "opacity"
let rippleLayerPositionKeyPath = "position"
let rippleLayerScaleKeyPath = "transform.scale"

extension CAMediaTimingFunction {
	static var `default`: CAMediaTimingFunction {
		return CAMediaTimingFunction(controlPoints: 0.4, 0, 0.2, 1)
	}
}

class RippleLayer: CAShapeLayer {
	
	private(set) var isStartAnimationActive = false
	
	var rippleTouchDownStartTime: TimeInterval = 0

	private var rippleRadius: CGFloat = 0
	
	override func setNeedsLayout() {
		super.setNeedsLayout()
		setRadii(with: bounds)
		setPathFromRadii()
		position = CGPoint(x: bounds.midX, y: bounds.midY)
	}
	
	private func setRadii(with rect: CGRect) {
		rippleRadius = hypot(rect.midX, rect.midY + expandRippleBeyondSurface)
	}
	
	private func setPathFromRadii() {
		let ovalRect = CGRect(
			x: bounds.midX - rippleRadius,
			y: bounds.midY - rippleRadius,
			width: rippleRadius * 2,
			height: rippleRadius * 2
		)
		let circelPath = UIBezierPath(ovalIn: ovalRect)
		path = circelPath.cgPath
	}
	
	func startRipple(at point: CGPoint, animated: Bool, completion: (() -> Void)?) {
		setPathFromRadii()
		opacity = 1
		position = CGPoint(x: bounds.midX, y: bounds.midY)
		if animated {
			isStartAnimationActive = true
		}
		
		let scaleAnimation = CABasicAnimation(keyPath: rippleLayerScaleKeyPath)
		scaleAnimation.fromValue = rippleStartingScale
		scaleAnimation.toValue = 1
		scaleAnimation.timingFunction = .default
		
		let centerPath = UIBezierPath()
		let startPoint = point
		let endPoint = CGPoint(x: bounds.midX, y: bounds.midY)
		centerPath.move(to: startPoint)
		centerPath.addLine(to: endPoint)
		centerPath.close()
		
		let positionAnimation = CAKeyframeAnimation(keyPath: rippleLayerPositionKeyPath)
		positionAnimation.path = centerPath.cgPath
		positionAnimation.keyTimes = [0, 1]
		positionAnimation.values = [0, 1]
		positionAnimation.timingFunction = .default
		
		let fadeInAnimation = CABasicAnimation(keyPath: rippleLayerOpacityKeyPath)
		fadeInAnimation.fromValue = 0
		fadeInAnimation.toValue = 1
		fadeInAnimation.duration = rippleFadeInDuration
		fadeInAnimation.timingFunction = CAMediaTimingFunction(name: .linear)
		
		CATransaction.begin()
		let animationGroup = CAAnimationGroup()
		animationGroup.animations = [scaleAnimation, positionAnimation, fadeInAnimation]
		animationGroup.duration = rippleTouchDownDuration
		CATransaction.setCompletionBlock {
			self.isStartAnimationActive = false
			completion?()
		}
		add(animationGroup, forKey: nil)
		rippleTouchDownStartTime = CACurrentMediaTime()
		CATransaction.commit()
	}
	
	func fadeInRipple(animated: Bool, completion: (() -> Void)?) {
		CATransaction.begin()
		let fadeInAnimation = CABasicAnimation(keyPath: rippleLayerOpacityKeyPath)
		fadeInAnimation.fromValue = 0
		fadeInAnimation.toValue = 1
		fadeInAnimation.duration = animated ? rippleFadeInDuration : 0
		fadeInAnimation.timingFunction = CAMediaTimingFunction(name: .linear)
		fadeInAnimation.fillMode = .forwards
		fadeInAnimation.isRemovedOnCompletion = false
		CATransaction.setCompletionBlock(completion)
		add(fadeInAnimation, forKey: nil)
		CATransaction.commit()
	}
	
	func fadeOutRipple(animated: Bool, completion: (() -> Void)?) {
		CATransaction.begin()
		let fadeOutAnimation = CABasicAnimation(keyPath: rippleLayerOpacityKeyPath)
		fadeOutAnimation.fromValue = 1
		fadeOutAnimation.toValue = 0
		fadeOutAnimation.duration = animated ? rippleFadeOutDuration : 0
		fadeOutAnimation.timingFunction = CAMediaTimingFunction(name: .linear)
		fadeOutAnimation.fillMode = .forwards
		fadeOutAnimation.isRemovedOnCompletion = false
		CATransaction.setCompletionBlock(completion)
		add(fadeOutAnimation, forKey: nil)
		CATransaction.commit()
	}
	
	func endRipple(animated: Bool, completion: (() -> Void)?) {
		let delay: TimeInterval = isStartAnimationActive ? rippleFadeOutDelay : 0
		CATransaction.begin()
		let fadeOutAnimation = CABasicAnimation(keyPath: rippleLayerOpacityKeyPath)
		fadeOutAnimation.fromValue = 1
		fadeOutAnimation.toValue = 0
		fadeOutAnimation.duration = animated ? rippleTouchUpDuration : 0
		fadeOutAnimation.beginTime = convertTime(rippleTouchDownStartTime + delay, from: nil)
		fadeOutAnimation.timingFunction = CAMediaTimingFunction(name: .linear)
		fadeOutAnimation.fillMode = .forwards
		fadeOutAnimation.isRemovedOnCompletion = false
		CATransaction.setCompletionBlock {
			completion?()
			self.removeFromSuperlayer()
		}
		add(fadeOutAnimation, forKey: nil)
		CATransaction.commit()
	}
}
