// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

struct Operation {

	var sourceName: String?
	var destinationName: String?

	let operationHash: String
	let id: Int
	let blockHash: String
	let level: Int
	let timestamp: Date // ISO8601
	let source: String
	let sourceManager: String?
	let destination: String
	let destinationManager: String?
	let amount: UInt64
	let fee: UInt64
}

extension Operation: Hashable {
	static func ==(lhs: Operation, rhs: Operation) -> Bool {
		return lhs.operationHash == rhs.operationHash
	}
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(operationHash)
	}
}

extension Operation: Decodable {

	private enum CodingKeys: String, CodingKey {
		case hash = "op_hash"
		case id
		case blockHash = "blk_hash"
		case level
		case timestamp
		case source = "src"
		case sourceManager = "src_mgr"
		case destination = "dst"
		case destinationManager = "dst_mgr"
		case amount
		case fee
	}
	
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		operationHash = try container.decode(String.self, forKey: .hash)
		id = try container.decode(Int.self, forKey: .id)
		blockHash = try container.decode(String.self, forKey: .blockHash)
		level = try container.decode(Int.self, forKey: .level)
		timestamp = try container.decode(Date.self, forKey: .timestamp)
		source = try container.decode(String.self, forKey: .source)
		sourceManager = try container.decodeIfPresent(String.self, forKey: .sourceManager)
		destination = try container.decode(String.self, forKey: .destination)
		destinationManager = try container.decodeIfPresent(String.self, forKey: .destinationManager)
		amount = UInt64(try container.decode(String.self, forKey: .amount)) ?? 0
		fee = UInt64(try container.decode(String.self, forKey: .fee)) ?? 0
	}
}
