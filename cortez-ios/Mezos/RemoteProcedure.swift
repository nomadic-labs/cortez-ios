// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation
import CryptoSwift

typealias Address = String
typealias PublicKey = String

struct Source {
	var address: Address
	var publicKey: PublicKey
}

enum Entrypoint {
	case `default`
	case root
	case `do`
	case setDelegate
	case removeDelegate
	case named(String)
}

extension Entrypoint: Equatable {
	static func == (lhs: Entrypoint, rhs: Entrypoint) -> Bool {
		switch (lhs, rhs) {
		case (.default, .default): return true
		case (.root, .root): return true
		case (.do, .do): return true
		case (.setDelegate, .setDelegate): return true
		case (.removeDelegate, .removeDelegate): return true
		case let (.named(lhsName), .named(rhsName)): return lhsName == rhsName
		default: return false
		}
	}
}

extension String {
	init(_ entrypoint: Entrypoint) {
		switch entrypoint {
		case .default: self = "default"
		case .root: self = "root"
		case .do: self = "do"
		case .setDelegate: self = "set_delegate"
		case .removeDelegate: self = "remove_delegate"
		case .named(let name): self = name
		}
	}
}

extension Entrypoint: Encodable {
	func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		try container.encode(String(self))
	}
}

struct Transfers {

	struct Destination {
		
		struct Parameters {
			let entrypoint: Entrypoint
			let expression: Expression
		}
	
		var address: Address
		var amount: UInt64
		var parameters: Parameters?
		
		init(address: Address, amount: UInt64, parameters: Parameters? = nil) {
			self.address = address
			self.amount = amount
			self.parameters = parameters
		}
	}

	var source: Source
	var destinations: [Destination]
}

extension Transfers: Encodable {
	
	enum CodingKeys: String, CodingKey {
		case source = "src"
		case sourcePublicKey = "src_pk"
		case destinations = "dsts"
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(source.address, forKey: .source)
		try container.encode(source.publicKey, forKey: .sourcePublicKey)
		try container.encode(destinations, forKey: .destinations)
	}
}

extension Transfers.Destination: Encodable {
	
	enum CodingKeys: String, CodingKey {
		case destination = "dst"
		case amount
		case entrypoint
		case parameters
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(address, forKey: .destination)
		try container.encode(String(amount), forKey: .amount)
		try container.encodeIfPresent(parameters?.entrypoint, forKey: .entrypoint)
		try container.encodeIfPresent(parameters?.expression, forKey: .parameters)
	}
}

struct Accounts {
	
	struct Destination {
		var balance: UInt64 = 0
		var delegate: Address?
		var script: Any
	}
	
	var source: Source
	var destinations: [Destination]
}

extension Accounts: Encodable {
	
	enum CodingKeys: String, CodingKey {
		case source = "src"
		case sourcePublicKey = "src_pk"
		case destinations = "dsts"
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(source.address, forKey: .source)
		try container.encode(source.publicKey, forKey: .sourcePublicKey)
		try container.encode(destinations, forKey: .destinations)
	}
}

extension Accounts.Destination: Encodable {
	
	enum CodingKeys: String, CodingKey {
		case credit
		case delegate
		case script
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(String(balance), forKey: .credit)
		try container.encodeIfPresent(delegate, forKey: .delegate)
		try container.encode(script, forKey: .script)
	}
}

struct Delegates {
	var source: Source
	var destinations: [Address]
}

extension Delegates: Encodable {

	enum CodingKeys: String, CodingKey {
		case source = "src"
		case sourcePublicKey = "src_pk"
		case destinations = "dsts"
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(source.address, forKey: .source)
		try container.encode(source.publicKey, forKey: .sourcePublicKey)
		try container.encode(destinations, forKey: .destinations)
	}
}

enum Network: String {
	case zeronet, babylonnet, carthagenet, alphanet, mainnet
}

enum Service: String {
	case mezos, tezos
}

extension URL {
	func appendingNetwork(_ network: Network) -> URL {
		return appendingPathComponent(network)
	}
	func appendingService(_ service: Service) -> URL {
		return appendingPathComponent(service)
	}
}

struct RemoteProcedure {

	let request: URLRequest
	
	init(_ request: URLRequest) {
		self.request = request
	}

	static func balance(at address: Address) -> RemoteProcedure {
		let pathComponents = [
			"chains", "main", "blocks", "head", "context", "contracts", address, "balance"
		]
		let url = baseURL
			.appendingService(.tezos)
			.appendingNetwork(network)
			.appendingPathComponents(pathComponents)
		return RemoteProcedure(URLRequest(url: url))
	}

	static func history<T: Sequence>(for addresses: T) -> RemoteProcedure where Address == T.Element {
		let queryItems = addresses.map { URLQueryItem(name: "ks", value: $0) }
		let url = baseURL
			.appendingService(.mezos)
			.appendingNetwork(network)
			.appendingPathComponent("history")
			.appendingQueryItems(queryItems)
		return RemoteProcedure(URLRequest(url: url))
	}
	static func history(for address: Address) -> RemoteProcedure {
		return history(for: CollectionOfOne(address))
	}

	static func forge(_ transfers: Transfers, encoder: JSONEncoder = .init()) throws -> RemoteProcedure {
		let url = baseURL
			.appendingService(.mezos)
			.appendingNetwork(network)
			.appendingPathComponent("forge_transfer")
		var request = URLRequest(url: url)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.httpBody = try encoder.encode(transfers)
		#if DEBUG
		if let body = request.httpBody.flatMap({ String(data: $0, encoding: .utf8) }) {
			debug(body)
		}
		#endif
		return RemoteProcedure(request)
	}
	
	static func inject(_ payload: Data) -> RemoteProcedure {
		let url = baseURL
			.appendingService(.tezos)
			.appendingNetwork(network)
			.appendingPathComponent("injection")
			.appendingPathComponent("operation")
		var request = URLRequest(url: url)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.httpBody = "\"\(payload.toHexString())\"".data(using: .ascii)
		return RemoteProcedure(request)
	}
	
	static func originate(_ accounts: Accounts, encoder: JSONEncoder = .init()) throws -> RemoteProcedure {
		let url = baseURL
			.appendingService(.mezos)
			.appendingNetwork(network)
			.appendingPathComponent("originate_account")
		var request = URLRequest(url: url)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.httpBody = try encoder.encode(accounts)
		return RemoteProcedure(request)
	}

	static func delegate(_ delegates: Delegates, encoder: JSONEncoder = .init()) throws -> RemoteProcedure {
		let url = baseURL
			.appendingService(.mezos)
			.appendingNetwork(network)
			.appendingPathComponent("change_delegate")
		var request = URLRequest(url: url)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.httpBody = try encoder.encode(delegates)
		return RemoteProcedure(request)
	}

	static func contracts<T: Sequence>(for addresses: T) -> RemoteProcedure where Address == T.Element {
		let queryItems = addresses.map { URLQueryItem(name: "mgrs", value: $0) }
		let url = baseURL
			.appendingService(.mezos)
			.appendingNetwork(network)
			.appendingPathComponent("contracts3")
			.appendingQueryItems(queryItems)
		return RemoteProcedure(URLRequest(url: url))
	}
	static func contracts(for address: Address) -> RemoteProcedure {
		return contracts(for: CollectionOfOne(address))
	}

	static func contractInfo<T: Sequence>(for addresses: T) -> RemoteProcedure where Address == T.Element {
		let queryItems = addresses.map { URLQueryItem(name: "ks", value: $0) }
		let url = baseURL
			.appendingService(.mezos)
			.appendingNetwork(network)
			.appendingPathComponent("contract_info")
			.appendingQueryItems(queryItems)
		return RemoteProcedure(URLRequest(url: url))
	}
	static func contractInfo(for address: Address) -> RemoteProcedure {
		return contractInfo(for: CollectionOfOne(address))
	}
}

extension Call {
	convenience init(remoteProcedure: RemoteProcedure, session: URLSession = .shared) {
		self.init(request: remoteProcedure.request, session: session)
	}
}
