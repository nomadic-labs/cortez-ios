// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

struct BinaryEncoder: Visitor {
	
	let sink: Sink

	init(_ sink: Sink) {
		self.sink = sink
	}

	func visit(_ integer: Int64) throws {
		try sink.write(UInt8(0x00))
		
		var magnitude: Int64 = abs(integer)
		
		// first byte handle sign flag
		var byte: UInt8 = UInt8(magnitude & 0x3f) | UInt8(integer.signum() == -1 ? 0x40 : 0) // make room for sign & next flag and set it
		magnitude >>= 6
		
		while magnitude != 0 {
			byte = byte | 0x80
			try sink.write(byte)
			byte = UInt8(magnitude & 0x7f)
			magnitude >>= 7
		}
		
		try sink.write(byte)
	}
	
	func visit(_ string: String) throws {
		try sink.write(UInt8(0x01))
		try encode(string)
	}
	
	func visit(_ sequence: [AnyVisitable]) throws {
		try sink.write(UInt8(0x02))
		try encode(sizeOf: sequence)
		try sequence.forEach { try $0.accept(self) }
	}
	
	func visit(_ primitive: Primitive) throws {
		let arguments = primitive.arguments ?? []
		
		let tag: UInt8 = {
			if arguments.count > 2 {
				return 0x09
			}
			return 2 * UInt8(arguments.count) + 3 + (primitive.annotations?.count ?? 0 == 0 ? 0 : 1)
		}()
		try sink.write(tag)
		
		try sink.write(primitive.name.code)
		
		if tag == 9 {
			try encode(sizeOf: arguments)
		}
		try arguments.forEach { try $0.accept(self) }
		
		if let annotations = primitive.annotations?.joined(separator: " ") {
			try encode(annotations)
		}
	}
	
	private func encode(size: Int) throws {
		try withUnsafeBytes(of: UInt32(truncatingIfNeeded: size).bigEndian ) {
			guard let buffer = $0.baseAddress?.assumingMemoryBound(to: UInt8.self) else { return }
			try sink.write(buffer, length: 4)
		}
	}
	
	private func encode(_ string: String) throws {
		let bytes = [UInt8](string.utf8)
		try encode(size: bytes.count)
		try sink.write(bytes, length: bytes.count)
	}
	
	private func encode(sizeOf sequence: [Visitable]) throws {
		let sink = StatSink()
		let encoder = BinaryEncoder(sink)
		try sequence.forEach { try $0.accept(encoder) }
		try encode(size: sink.count)
	}
}

private final class StatSink: Sink {
	
	private(set) var count: Int = 0
	
	func write(_ buffer: UnsafePointer<UInt8>, length: Int) throws {
		count += length
	}
}
