// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

struct Expression {

	let statements: [AnyVisitable]
	
	init<T: Visitable>(_ statements: [T]) {
		self.statements = statements.map(AnyVisitable.init)
	}
	init<T: Visitable>(_ statements: T...) {
		self.init(statements)
	}

	static var drop = Expression(Primitive(name: .DROP))
	static var operation = Expression(Primitive(name: .operation))
	static var implicitAccount = Expression(Primitive(name: .IMPLICIT_ACCOUNT))
	static var unit = Expression(Primitive(name: .unit))
	static var transferTokens = Expression(Primitive(name: .TRANSFER_TOKENS))
	static var cons = Expression(Primitive(name: .CONS))
	static var failWith = Expression(Primitive(name: .FAILWITH))
	static var UNIT = Expression(Primitive(name: .UNIT))
	static var assertSome: Expression {
		.sequence(of: ifNone(.sequence(of: .flatten(.UNIT, .failWith)), else: .sequence()))
	}

	static var setDelegate = Expression(Primitive(name: .SET_DELEGATE))
	
	static func flatten(_ sequence: [Expression]) -> Expression {
		Expression(sequence.flatMap { $0.statements })
	}
	static func flatten(_ sequence: Expression...) -> Expression {
		flatten(sequence)
	}
	static func flatten(_ generate: () -> [Expression]) -> Expression {
		flatten(generate())
	}
	
	static func sequence(of statements: [Expression]) -> Expression {
		Expression(statements.map { $0.statements })
	}
	static func sequence(of statements: Expression...) -> Expression {
		sequence(of: statements)
	}

	static func `nil`(_ argument: Expression) -> Expression {
		Expression(Primitive(name: .NIL, arguments: argument.statements))
	}

	struct ValueType {

		let primitive: Primitive
	
		static let address = ValueType(primitive: .init(name: .address))
		static let keyHash = ValueType(primitive: .init(name: .key_hash))
		static let mutez = ValueType(primitive: .init(name: .mutez))
	}
	
	
	struct Value {

		let type: ValueType
		let value: AnyVisitable
		
		static func address(_ value: String) -> Value {
			Value(type: .address, value: AnyVisitable(value))
		}
		static func keyHash(_ value: String) -> Value {
			Value(type: .keyHash, value: AnyVisitable(value))
		}
		static func mutez(_ value: UInt64) -> Value {
			Value(type: .mutez, value: AnyVisitable(Int64(truncatingIfNeeded: value)))
		}
	}
	
	static func push(_ value: Value) -> Expression {
		Expression(Primitive(name: .PUSH, arguments: [AnyVisitable(value.type.primitive), value.value]))
	}
	
	static func contract(_ parameterType: Expression) -> Expression {
		Expression(Primitive(name: .CONTRACT, arguments: parameterType.statements))
	}
	
	static func ifNone(_ success: Expression, else failure: Expression) -> Expression {
		Expression(Primitive(name: .IF_NONE, arguments: [success, failure].map { $0.statements }))
	}
	
	static func none(_ type: ValueType) -> Expression {
		Expression(Primitive(name: .NONE, arguments: [type.primitive]))
	}
	static func some(_ value: Value) -> Expression {
		.sequence(of: .push(value), Expression(Primitive(name: .SOME)))
	}
}

extension Expression: Encodable {
	func encode(to encoder: Encoder) throws {
		var container = encoder.unkeyedContainer()
		try container.encode(contentsOf: statements)
	}
}
