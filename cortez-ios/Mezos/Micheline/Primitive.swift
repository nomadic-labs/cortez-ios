// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

struct Primitive {

	enum Name: String, CaseIterable {
		case parameter
		case storage
		case code
		case False
		case Elt // 4
		case Left
		case None
		case Pair
		case Right // 8
		case Some
		case True
		case Unit
		case PACK
		case UNPACK
		case BLAKE2B
		case SHA256
		case SHA512 // 16
		case ABS
		case ADD
		case AMOUNT
		case AND
		case BALANCE
		case CAR
		case CDR
		case CHECK_SIGNATURE
		case COMPARE
		case CONCAT
		case CONS
		case CREATE_ACCOUNT
		case CREATE_CONTRACT
		case IMPLICIT_ACCOUNT
		case DIP
		case DROP // 32
		case DUP
		case EDIV
		case EMPTY_MAP
		case EMPTY_SET
		case EQ
		case EXEC
		case FAILWITH
		case GE
		case GET
		case GT
		case HASH_KEY
		case IF
		case IF_CONS
		case IF_LEFT
		case IF_NONE
		case INT
		case LAMBDA
		case LE
		case LEFT
		case LOOP
		case LSL
		case LSR
		case LT
		case MAP
		case MEM
		case MUL
		case NEG
		case NEQ
		case NIL
		case NONE
		case NOT
		case NOW // 64
		case OR
		case PAIR
		case PUSH
		case RIGHT
		case SIZE
		case SOME
		case SOURCE
		case SENDER
		case SELF
		case STEPS_TO_QUOTA
		case SUB
		case SWAP
		case TRANSFER_TOKENS
		case SET_DELEGATE
		case UNIT
		case UPDATE
		case XOR
		case ITER
		case LOOP_LEFT
		case ADDRESS
		case CONTRACT
		case ISNAT
		case CAST
		case RENAME
		case bool
		case contract
		case int
		case key
		case key_hash
		case lambda
		case list
		case map
		case big_map
		case nat
		case option
		case or
		case pair
		case set
		case signature
		case strin
		case bytes
		case mutez
		case timestamp
		case unit
		case operation
		case address
		case SLICE
		case DIG
		case DUG
		case EMPTY_BIG_MAP
		case APPLY
		case chain_id
		case CHAIN_ID // 117
		
		var code: UInt8 {
			struct Static {
				static let index = [Name: UInt8](uniqueKeysWithValues: Name.allCases.enumerated().map { ($1, UInt8($0)) })
			}
			guard let index = Static.index[self] else { fatalError() }
			return index
		}
	}

	let name: Name
	let arguments: [AnyVisitable]?
	let annotations: [String]?
	
	init<T: Visitable>(name: Name, arguments: [T]? = nil, annotations: [String]? = nil) {
		self.name = name
		self.arguments = arguments?.map(AnyVisitable.init)
		self.annotations = annotations
	}
	init(name: Name, annotations: [String]? = nil) {
		self.name = name
		self.arguments = nil
		self.annotations = annotations
	}
}

extension Primitive: Visitable {
	func accept(_ visitor: Visitor) throws {
		try visitor.visit(self)
	}
}
