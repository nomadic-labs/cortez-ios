// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

protocol Visitable: Encodable {
	func accept(_ visitor: Visitor) throws
}

extension Int64: Visitable {
	func accept(_ visitor: Visitor) throws {
		try visitor.visit(self)
	}
}

extension String: Visitable {
	func accept(_ visitor: Visitor) throws {
		try visitor.visit(self)
	}
}

extension Array: Visitable where Element == AnyVisitable {
	func accept(_ visitor: Visitor) throws {
		try visitor.visit(self)
	}
}

struct AnyVisitable: Visitable {
	
	private let base: Visitable

	init<T: Visitable>(_ base: T) {
		self.base = base
	}
	
	func accept(_ visitor: Visitor) throws {
		try base.accept(visitor)
	}
	func encode(to encoder: Encoder) throws {
		try base.encode(to: encoder)
	}
}
