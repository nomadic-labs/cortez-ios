
[www.umamiwallet.com](https://www.umamiwallet.com/?utm_source=cortezios&utm_medium=gitlab)
----
<b> {+ Umami-Wallet+} </b>is the new wallet developped by Nomadic-labs.

# This project is deprecated

## App Architecture

Cortez iOS try to use the principles described in [the Clean Swift Handbook](https://clean-swift.com/handbook/) which are derived from the Clean Architecture proposed by Uncle Bob. This architecture make use of a unidirectional control flow called **VIP**.

<img src="https://clean-swift.com/wp-content/uploads/2015/08/VIP-Cycle-1024x768.png" style="width:50%"/>

A typical cycle follow these steps:

1. Trigger a use case from a user input, a button for exemple.
2. The **view controller** send a **request** to the interactor to execute some business logic.
3. The **interactor** send a **response** to the presenter which will format the result to a more display friendly form.
4. The **presenter** send a **view model** to the view controller for display.

An interactor is usually composed of workers which implement a specific step of the business logic, for exemple a call to a web service.

Transitions are handled by a two steps routing process: **data passing** and **navigation**. A view controller keep a reference to a router for any navigation needs.

## Source code organization

The folder hierarchy mirrors the features provided by the app:

* Create/Restore an address (**CreateWallet**)
* Send/Request tez (**Transfer**)
* Explore your transaction history (**ListOperations**)
* Manage your smart contracts (**ListAccounts**)
* Manage your contacts (**ListContacts**)
* Edit your security settings (**ListSettings**)

**Mezos** contains everything needed to interact with the mezos indexer. **Onboard** is a helper component which make forms creation easier and coherent between screens.

The **business framework** is a collection of tools used for every asynchronous operations. Its use should be deprecated in favor of the newly introduced apple framework: [Combine](https://developer.apple.com/documentation/combine).

**Goodies** contains neet class helpers shared between features.
