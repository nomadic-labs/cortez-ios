// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import XCTest

@testable import cortez_ios

class ConfirmBackupPhraseViewControllerTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
	
	func testRequestValidationWhenChangingTextViewText() {
		// given
		let spy = ConfirmBackupPhraseInteractorSpy()
		let dummy = UITextView(frame: .zero)
		dummy.text = ""
		let stub = SuggestionControl()
		stub.textView = dummy
		let sut = ConfirmBackupPhraseViewController()
		sut.interactor = spy

		// when
		// TODO

		// then
		//XCTAssertTrue(spy.backupPhraseValidationExecuteCalled)
	}
	
	final class ConfirmBackupPhraseInteractorSpy: ConfirmBackupPhraseBusinessLogic {

		var backupPhraseValidationExecuteCalled = false

		func execute(_ request: Wallet.BackupPhrase.Validation.Request) {
			backupPhraseValidationExecuteCalled = true
		}
	}
}
