// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import XCTest

@testable import cortez_ios

class ConfirmBackupPhraseInteractorTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
	
	func testBackupPhraseConfirmationForwarding() {
		let spy = ConfirmBackupPhrasePresenterSpy()
		let sut = ConfirmBackupPhraseInteractor(
			validator: BackupPhraseVerifierStub(expected: true),
			presenter: spy
		)
		sut.backupPhrase = ""
		
		// when
		sut.execute(Wallet.BackupPhrase.Validation.Request(backupPhrase: ""))
		
		// then
		XCTAssertTrue(spy.backupPhraseValidationFormatCalled)
	}
	
	func testBackupPhraseConfirmationForwardingWhenStoreEmpty() {
		let spy = ConfirmBackupPhrasePresenterSpy()
		let sut = ConfirmBackupPhraseInteractor(
			validator: BackupPhraseVerifierStub(expected: true),
			presenter: spy
		)
		sut.backupPhrase = nil
		
		// when
		sut.execute(Wallet.BackupPhrase.Validation.Request(backupPhrase: ""))
		
		// then
		XCTAssertFalse(spy.backupPhraseValidationFormatCalled)
	}
	
	struct BackupPhraseWordSuggesterStub: BackupPhraseWordSuggestionLogic {
		
		let expected: [String]

		func suggestWords(forPrefix prefix: String, completion: @escaping ([String]) -> Void) {
			completion(expected)
		}
	}

	struct BackupPhraseWordSuggesterDummy: BackupPhraseWordSuggestionLogic {
		func suggestWords(forPrefix prefix: String, completion: @escaping ([String]) -> Void) {}
	}
	
	struct BackupPhraseVerifierStub: BackupPhraseCompareLogic {

		let expected: Bool

		func compare(_ backupPhrase: String, to reference: String, completion: @escaping (Bool) -> Void) {
			completion(expected)
		}
	}
	
	struct BackupPhraseVerifierDummy: BackupPhraseCompareLogic {
		func compare(_ backupPhrase: String, to reference: String, completion: @escaping (Bool) -> Void) {}
	}
	
	struct WalletCreatorStub: WalletCreationLogic {
		
		let expected: Error?
		
		func create(withBackupPhrase backupPhrase: String, password: String, completion: @escaping (Error?) -> Void) {
			completion(expected)
		}
	}
	
	struct WalletCreatorDummy: WalletCreationLogic {
		func create(withBackupPhrase backupPhrase: String, password: String, completion: @escaping (Error?) -> Void) {}
	}

	final class ConfirmBackupPhrasePresenterSpy: ConfirmBackupPhrasePresentationLogic {

		var backupPhraseFormatCalled = false
		var backupPhraseValidationFormatCalled = false

		func format(_ response: Wallet.BackupPhrase.Response) {
			backupPhraseFormatCalled = true
		}

		func format(_ response: Wallet.BackupPhrase.Validation.Response) {
			backupPhraseValidationFormatCalled = true
		}
	}
}
